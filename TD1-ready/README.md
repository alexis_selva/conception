# Devoir 1 : algorithmes de compression

La compression de données est une problématique essentielle en informatique: elle consiste à représenter une donnée dans un format compact, qui permet de réduire l'espace occupé. On dit que la donnée est stockée dans un format compressé.

Les formats compressés sont partout présents. Un DVD contient une vidéo compressée. Votre appareil photo stocke les images dans un format compressé. Votre lecteur audio contient des titres dans un format compressé également. Votre téléphone portable lui aussi compresse les communications audio avant de les transmettre à la tour GSM. Les pages web sont transmises dans un format compressé à votre navigateur. Toutes ces applications seraient infaisables sans algorithmes de compression : en effet, la taille des données brutes, c'est-à-dire non compressées, est tout simplement trop grande pour être stockée sur un DVD, dans votre lecteur audio ; pour être transmises à un débit raisonnable au réseau téléphonique ; ou encore pour être transmises via le réseau 3G assez rapidement pour que la page s'affiche sans trop de délai.

On distinguera plusieurs critères quant au choix d'un algorithme de compression. On pourra vouloir un algorithme de compression sans perte : typiquement, il n'est pas acceptable de dégrader du texte lors de la compression. C'est la raison pour laquelle les pages web, par exemples, sont usuellement compressées avec l'algorithme GZIP ou Deflate, des algorithmes de compression sans perte. Au contraire, pour des images, de la musique, ou des vidéos, il sera généralement acceptable de dégrader quelque peu la qualité du document, dans les limites du raisonnable. Cela permet de faire appel à des algorithmes spécialisés qui offrent un meilleur ratio de compression en utilisant les propriétés sous-jacentes du format : l'algorithme MP3 élimine les fréquences inaudibles par l'oreille humaine, et un algorithme de compression vidéo utilise de la détection de mouvements pour représenter une scène de manière plus compacte.

Les images, par exemples, peuvent être réprésentées ou bien dans un format sans perte (TIFF, PNG) ou dans un format avec perte (JPEG,...), en fonction du contexte d'utilisation.

Un autre critère de choix est la vitesse de décompression : sur des machines aux capacités limitées, comme par exemple des téléphones portables, il n'est pas acceptable de compresser les images dans un format qui nécessite trop de calculs pour être décompressé. Il en résulterait une consommation d'énergie et des délais excessifs.

Les algorithmes de compression constituent un sujet de recherche qui est encore actif à l'heure actuelle. On cherche actuellement un remplaçant au format JPEG, largement utilisé sur le web mais peu performant en termes de qualité.

Obtenir le sujet / soumission des résultats
Pour compléter ce devoir, vous devrez écrire du code source. Un squelette de programme vous est fourni dans une archive. L'archive est disponible en ligne. Téléchargez-la, puis extrayez son contenu dans le dossier de votre choix. La soumission des résultats se fait comme lors de la semaine 0.

Structure du devoir
Ce devoir est divisé en deux parties. Dans la première, nous implémenterons l'un des tout premiers algorithmes de compression sans perte, l'algorithme RLE. Ceci permettra de vous familiariser avec la manipulation des tableaux et l'écriture de boucles non-triviales.

Dans une seconde partie, nous verrons un algorithmique historique, LZ77, précurseur du format LZW. LZW est encore utilisé de nos jours pour compresser les images au format GIF ou TIFF, et les documents PDF. LZ77 est utilisé dans le système de fichiers NTFS de Microsoft, ou encore dans l'algorithme Deflate, en combinaison avec un codage de Huffman.

Ce devoir est long et relativement difficile. Celui de la semaine suivante sera plus court, et aussi plus simple. Essayez pour cette semaine de répondre au plus grand nombre possible de questions. La première partie, sur l'algorithme RLE, est relativement aisée, et vous devriez arriver à la compléter sans trop de problèmes. Elle vous donnera 20 points, pour un total de 45. Pour avoir la moyenne, il vous faudra donc répondre au moins à la première question de la seconde partie, sur la recherche d'occurrences dans l'algorithme LZ77.

Rendu du devoir
Ce devoir est à terminer pour le dimanche 17 novembre à 12h00. Si vous échouez à rendre le devoir dans les temps, vous pourrez continuer de soumettre vos réponses la semaine suivante, mais vous n'obtiendrez que 40% des points.

Correction
Nous mettrons en ligne la correction après la fin de la période de soumission : vous retrouverez dans l'archive de correction un code source qui fonctionne, et qui est abondamment commenté.

En cas de problème
Si vous rencontrez des soucis techniques avec le processus de soumission, ou si le sujet vous semble ambigu ou contenir une erreur, les forums du cours sont à votre disposition pour demander de l'aide.

Notations
Les algorithmes de compression travaillent sur des séquences de bits. On notera une telle séquence à l'aide d'accolades, par analogie avec la syntaxe Java d'initialisation des tableaux en ligne : { 0, 1, ... }.

L'algorithme RLE
L'algorithme RLE est relativement bien adapté aux formats d'image noir-et-blanc. Son fonctionnement peut s'expliquer de manière intuitive : au lieu de représenter {0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0}, on peut compresser ce tableau de neuf éléments en un tableau de six éléments {0, 5, 1, 1, 0, 5}, qui peut se comprendre comme « répéter 0 cinq fois, répéter 1 une fois, répéter 0 cinq fois ».

Dans ce devoir, notre critère de compression sera la taille du tableau en Java. Dans une implémentation réelle, on utiliserait probablement une représentation binaire efficace qui minimise la place gaspillée. En effet, dans le second tableau, les éléments 0 et 1 ne prennent toujours qu'un seul bit, mais utiliseront autant de place qu'un entier 32-bits. Nous ne traiterons pas dans ce sujet la question du format binaire efficace de stockage.

Les tests automatiques utilisent des tableaux de grande taille. N'hésitez pas, pour cette section, à tester vos algorithmes sur des tableaux plus petits, afin de vérifier les résultats à la main. Lorsque vous arriverez à la dernière question, il pourra être utile de vérifier manuellement que RLEInverse(RLE(t)) est bien égal, en longueur et pour chaque élément, à t lui-même.

Longueur de compression
Créez une classe RLE, et écrivez la méthode statique public static int longueurRLE(int[] t) qui calcule la longueur nécessaire pour représenter au format RLE le tableau de bits t. Dans l'exemple ci-dessus, votre fonction doit renvoyer 6.

Votre fonction devra faire quelque chose d'approprié pour les cas limites. Un tableau de longueur zéro, une fois encodé, restera un tableau de longueur zéro. Un tableau null, en revanche, constitue une entrée invalide : vous pourrez lancer une erreur fatale, comme par exemple IllegalArgumentException. Nous garderons ces conventions tout au long du sujet.

Ouvrez le fichier Test.java. Décommentez le contenu de testRle1. Exécutez la classe Test et soumettez votre résultat.

Compression
Il s'agit maintenant d'écrire la fonction public static int[] RLE(int[] t) qui compresse t au format RLE et renvoie le résutat. Cette fonction fera naturellement appel à la fonction longueurRLE() pour connaître la taille du tableau de destination à créer, c'est-à-dire le tableau qui contiendra la version compressée.

Décommentez son contenu et soumettez les résultats de la fonction testRle2().

Décompression
Maintenant que votre algorithme est capable de compresser des données correctement au format RLE, il s'agit de les décompresser. Pour ce faire, nous allons procéder de la même manière que pour la compression. Écrivez d'abord public static int longueurRLEInverse(int[] t). Cette fonction prend un tableau t au format RLE, et calcule la longueur nécessaire pour le stocker une fois décompressé.

Décommentez son contenu et soumettez les résultats de la fonction testRle3().

Enfin, écrivez la fonction public static int[] RLEInverse(int[] t) qui prend un tableau t au format RLE et renvoie sa version décompressée.

Décommentez son contenu et soumettez les résultats de la fonction testRle4().

Le mot de la fin
Vous constaterez que pour notre batterie de tests automatiques, il n'est pas clair que les données compressées soient plus courtes que les données brutes. En effet, les données brutes ont été générées aléatoirement, et les successions d'un même bit sont rares. En pratique, RLE est appliqué à des problèmes précis, comme des images noir et blanc. Si un bit représente un pixel, alors l'image sera essentiellement composée de très longues séquences consécutives de 0 et de 1, ce qui en fait un bon candidat pour une compression via RLE.

L'algorithme LZ77
LZ77 est un algorithme plus sophistiqué. Il se comporte beaucoup mieux de manière générale, et produit un meilleur ratio de compression. Il est aussi plus difficile à comprendre et à implémenter.

Nous implémentons une version tout ce qu'il y a de plus standard de l'algorithme LZ77 : n'hésitez donc pas à chercher sur Internet des informations supplémentaires quant au fonctionnement de l'algorithme, si les explications fournies dans le sujet vous semble incomplètes.

Conseils généraux
Cet algorithme est délicat. N'hésitez pas à prendre l'initiative, et à écrire des fonctions de débogage pour afficher vos tableaux, ainsi que des fonctions toString pour pouvoir afficher vos différentes classes. Écrivez des tests unitaires sur des tableaux de petite taille, et vérifiez les résultats manuellement.

Fonctionnement de LZ77
On pourrait résumer LZ77 à l'aide de la maxime « l'histoire se répète ». En d'autres termes, LZ77 va chercher des similitudes entre le futur et le passé. Détaillons un peu ces grandes idées.

On cherche à compresser un tableau d'entrées. Lorsque l'algorithme est en cours, il maintient une position courante dans le tableau d'entrées. La position courante marque le début des caractères à venir (la partie du tableau d'entrées qui n'a pas encore été compressée) ; la position courante marque également la fin de la fenêtre, qui est constituée des N caractères précédant la position courante.


{ 1, 0, 1, 0, 1, 1, 1 } ← tableau d'entrées
           ↑
                    position
                             courante
                             Dans l'exemple ci-dessus, les caractères à venir sont { 0, 1, 1, 1 } et la fenêtre, que l'on suppose de taille 100, est { 1, 0, 1 }, car elle est bornée par le début du tableau d'entrées.
                             
                             LZ77 cherche des similitudes entres les caractères à venir et la fenêtre. Par exemple, la séquence { 0, 1 } est présente à la position courante, mais aussi dans la fenêtre, deux caractères plus tôt. On dit que l'on a trouvé une occurrence. Cette occurrence est caractérisée par sa taille, qui vaut 2, et son retour, c'est-à-dire sa position par rapport à la position courante. Ici, l'occurrence se trouve deux caractères plus tôt que la position courante : elle a donc un retour de deux.
                             
                             Quand on dit que l'on cherche une occurrence, on cherche toujours une occurrence qui commence à la position courante.
                             
                             Structures de données
                             Éditez le fichier LZ77.java, et commencez par créer une classe Occurrence, qui se contente d'offrir deux champs taille et retour, ainsi qu'un constructeur Occurrence(int retour, int taille). N'oubliez pas que la lettre r possède deux occurrences dans le mot occurrences.
                             
                             Recherche d'occurrence
                             Créez la classe LZ77. Il s'agit maintenant d'écrire la fonction suivante :
                             
                               public static Occurrence plusLongueOccurrence(
                                     int[] t,
                                           int positionCourante,
                                                 int tailleFenetre
                                                     ) {
                                                     Cette fonction prend un tableau d'entrées t, la position courante dans le tableau, et la taille de la fenêtre. Elle renvoie la plus grande Occurrence trouvée dans la fenêtre. N'oubliez pas que le caractère courant ne fait pas partie de la fenêtre.
                                                     
                                                     Le cas où aucune occurrence n'est trouvée correspond à une Occurrence de taille 0 et de retour 0. Dans le cas où plusieurs occurrences de même taille sont présentes dans la fenêtre, cette fonction devra choisir l'occurrence la plus à gauche, c'est-à-dire la plus éloignée de la position courante.
                                                     
                                                     Naturellement, plus la fenêtre sera grande, plus la fonction aura de travail à effectuer. Dans la suite de ce devoir, nous prendrons une taille de fenêtre égale à 100.
                                                     
                                                     Cette fonction doit effectuer bon nombre de manipulations sur les tableaux. Il est facile d'y glisser des erreurs par inadvertance. Il sera donc opportun de réfléchir sur papier avant de se lancer dans l'implémentation.
                                                     
                                                     Quels sont les hypothèses de la fonction ?
                                                     Que doit-il se passer dans les cas limites ? (Au tout début du tableau, à la toute fin du tableau.)
                                                     Quelle est l'organisation générale de la fonction ? Combien de boucles imbriquées ?
                                                     Décommentez son contenu et soumettez les résultats de la fonction testLz1().
                                                     
                                                     Fonctionnement de l'algorithme LZ77 (suite)
                                                     Voyons maintenant comment fonctionne précisément l'algorithme LZ77.
                                                     
                                                     On commence au début du tableau d'entrées.
                                                     On trouve la plus grande occurrence o, de taille k.
                                                     On stocke o ainsi que le caractère à venir situé immédiatement après l'occurrence o.
                                                     On avance de k + 1 éléments, et on recommence à l'étape 2.
                                                     Si l'on reprend l'exemple précédent :
                                                     
                                                               caractère situé
                                                                         après l'occurrence
                                                                                          ↓
                                                                                          { 1, 0, 1, 0, 1, 1, 1 } ← tableau d'entrées
                                                                                                     ↑        ↑
                                                                                                              position   position
                                                                                                                       courante   courante
                                                                                                                                           à l'étape
                                                                                                                                                               suivante
                                                                                                                                                               À ce stade-là, l'algorithme stockera l'occurrence (2, 2) et le caractère 1. Le schéma montre la position courante à l'étape suivante (c'est-à-dire une fois l'occurrence et le caractère stockés).
                                                                                                                                                               
                                                                                                                                                               Il est crucial de stocker le caractère suivant. En effet, il n'est pas toujours possible de trouver une occurrence. Il faut malgré tout stocker le caractère situé à la position courante. Garder le caractère situé après l'occurrence permet d'assurer cette garantie.
                                                                                                                                                               
                                                                                                                                                               Structures de données (suite)
                                                                                                                                                               Créez une classe Element. Elle contiendra deux champs : un pour mémoriser l'occurrence et un autre pour mémoriser caractère suivant. Elle fournira le constructeur Element(Occurrence e, int s).
                                                                                                                                                               
                                                                                                                                                               Ainsi, le résultat de l'encodage sera une succession d'éléments que nous stockerons sous forme de tableau Element[].
                                                                                                                                                               
                                                                                                                                                               Convention de terminaison
                                                                                                                                                               Nous ferons désormais l'hypothèse que nos entreés sont des successions de 0 et de 1, terminés par un caractère spécial, ici un 2, qui marque la fin de la chaîne. N'oubliez pas de terminer vos tableaux de test par l'entier 2.
                                                                                                                                                               
                                                                                                                                                               On peut voir l'intérêt de cette convention en considérant le tableau { 1, 1 }. À la position 1, on voudra créer l'occurrence (1, 1). Il faudra alors accéder à l'élément suivant, qui n'existe pas car c'est la fin du tableau. Rajouter un 2 à la fin de toutes les entrées permet de garantir que le 2 ne fera jamais partie d'aucune occurrence et sera toujours un caractère suivant valide.
                                                                                                                                                               
                                                                                                                                                               Dans la suite des questions, considérez que le 2 fait partie intégrante du tableau : il doit être compté pour la longueur, doit être compressé comme les autres caractères, et doit apparaître à la fin de la chaîne décompressée.
                                                                                                                                                               
                                                                                                                                                               Longueur de la chaîne compressée
                                                                                                                                                               Écrivez la fonction public static int LZ77Longueur(int[] t, int tailleFenetre) qui calcule le nombre d'éléments nécessaire pour représenter t une fois compressé au format LZ77. Il faudra faire appel à plusLongueOccurrence. Attention à bien avancer du bon nombre de cases à chaque recherche d'occurrence.
                                                                                                                                                               
                                                                                                                                                               Décommentez son contenu et soumettez les résultats de la fonction testLz2().
                                                                                                                                                               
                                                                                                                                                               Compression
                                                                                                                                                               La fonction de compression est très similaire au calcul de la longueur de la chaîne compressée, si ce n'est qu'elle stocke le résultat (compressé) dans un tableau d'Element.
                                                                                                                                                               
                                                                                                                                                               Écrivez la fonction public static Element[] LZ77(int[] tab, int tailleFenetre) qui se contente de compresser le tableau tab et de renvoyer la version compressée.
                                                                                                                                                               
                                                                                                                                                               Afin de valider vos résultats, il est nécessaire que vous les affichiez dans un format précis. À cet effet, écrivez une fonction public static void afficheEncode(Element[] tab) qui affiche tab sur la sortie standard, suivi d'un retour chariot \n. Les fonctions de test se chargeront d'appeler afficheEncode comme il faut.
                                                                                                                                                               
                                                                                                                                                               La fonction d'affichage suit des conventions précises. Chaque Element de retour r, de taille t et de suivant s devra être affiché (r,t)s. Les Elements devront être affichés les uns à la suite des autres, sans séparateur. À titre indicatif, voici le début de la sortie attendue pour le premier test de la fonction testLz3():
                                                                                                                                                               
                                                                                                                                                               (0,0)0(1,1)0(0,0)1(4,4)1(2,2)0(10,3)1
                                                                                                                                                               Décommentez son contenu et soumettez les résultats de la fonction testLz3().
                                                                                                                                                               
                                                                                                                                                               Longueur de la chaîne décompressée
                                                                                                                                                               Écrivez la fonction public static int LZ77InverseLongueur(Element[] t). Elle prend une chaîne compressée, c'est-à-dire un tableau d'Element. Elle renvoie la longueur qu'occupera t une fois décompressé.
                                                                                                                                                               
                                                                                                                                                               Cette fonction est relativement simple à écrire, car l'information de longueur est contenue dans chaque Occurrence.
                                                                                                                                                               
                                                                                                                                                               Décommentez son contenu et soumettez les résultats de la fonction testLz4().
                                                                                                                                                               
                                                                                                                                                               Décompression
                                                                                                                                                               Écrivez la fonction public static int[] LZ77Inverse(Element[] t). Elle prend un tableau t représentant des données compressées et renvoie la version décompressée. Vous aurez besoin d'implémenter l'opération void blit(int[] t1, int[] t2, int start1, int len, int start2), qui copie les caractères du tableau t1 allant de start1 à start1 + len - 1 vers le tableau t2 à partir de la position start2. N'hésitez pas à tester vos fonctions sur des exemples de petite taille.
                                                                                                                                                               
                                                                                                                                                               Afin de valider vos résultats, il est nécessaire que vous les affichiez dans un format précis. À cet effect, écrivez une fonction public static void afficheDecode(int[] t) qui affiche t sur la sortie standard, suivi d'un retour chariot \n. Les entiers devront être affichés séparés par un espace. À titre indicatif, voici le début de la sortie attendue pour le premier test de la fonction testLz5():
                                                                                                                                                               
                                                                                                                                                               1 0 1 0 1 0 0 0 0 1 0 1 1 1 0 0 1 1 1 1
                                                                                                                                                               Décommentez son contenu et soumettez les résultats de la fonction testLz5().
