// Classe RLE 
public class RLE {
          
    // Constructeur
    public RLE () {
    }

    // Fonction retournant la taille du tableau necessaire a une compression RLE
    public static int longueurRLE(int[] t){
        
        // Verifier le parametre
        if (t == null){
            throw new IllegalArgumentException("Le tableau d entiers specifie en entree est nul");
        } else if (t.length == 0) {
            return 0;
        }
        
        // Declarer une taille nulle
        int taille = 0;
        
        // Lire le tableau du 1er entier a l avant dernier
        for (int i = 0 ; i < (t.length - 1) ; i++){
        
            // Comparer la valeur actuelle avec la suivante
            if (t[i] != t[i + 1]){
                
                // Mettre a jour la taille
                taille += 2;
            }
        }
        
        // Mettre a jour la taille pour le dernier entier
        taille += 2;
        
        // Retourner la taille
        return taille;
    }
    
    // Fonction compressant un tableau d entier en utilisant le format RLE
    public static int[] RLE(int[] t){
    
        // Verifier le parametre
        if (t == null){
            throw new IllegalArgumentException("Le tableau d entiers specifie en entree est nul");
        }
    
        // Determiner la taille du tableau a retourner
        int taille = longueurRLE(t);
        
        // Declarer le tableau a retourner
        int[] compression = new int[taille];
        
        // Verifier que la taille du tableau est positive
        if (taille > 0){
            
            // Declarer le nombre de repetitions
            int repetitions = 1;
            
            // Declare la position courante dans le tableau
            int pos = 0;
            
            // Compresser le tableau d entier
            for (int i = 0 ; i < (t.length - 1) ; i++){
                
                // Comparer la valeur courante et la suivante
                if (t[i] != t[i + 1]){
                
                    // Ajouter la valeur courante
                    compression[pos++] = t[i];
                    
                    // Ajouter le nombre de repetitions
                    compression[pos++] = repetitions;
                    
                    // Reset the number of repetitions
                    repetitions = 1;
                            
                } else {
                    
                    // Incrementer le nombre de repetitions
                    repetitions++;
                }
            }
            
            // Ajouter les informations relatives au dernier entier
            compression[pos++] = t[t.length - 1];
            compression[pos++] = repetitions;
        }
            
        // Retourner le tableau d entier compresse
        return compression;
    }
    
    // Fonction retournant la taille du tableau necessaire pour la decompression RLE
    public static int longueurRLEInverse(int[] t){
    
        // Verifier le parametre
        if (t == null){
            throw new IllegalArgumentException("Le tableau d entiers specifie en entree est nul");
        } else if (t.length == 0) {
            return 0;
        }
        
        // Declarer la taille du tableau
        int taille = 0;

        // Parcourir le tableau de 2 en 2 du 2eme bit au dernier 
        for (int i = 1 ; i < t.length ; i += 2){
        
            // Mettre a jour la taille
            taille += t[i];
        }
        
        // Retourner la taille
        return taille;
    
    }

    // Fonction decompressant un tableau d entier au format RLE
    public static int[] RLEInverse(int[] t){
    
        // Verifier le parametre
        if (t == null){
            throw new IllegalArgumentException("Le tableau d entiers specifie en entree est nul");
        }
        
        // Determiner la taille du tableau a retourner
        int taille = longueurRLEInverse(t);
        
        // Declarer le tableau a retourner
        int[] decompression = new int[taille];
        
        // Verifier que la taille est positive
        if (taille > 0){
            
            // Declarer la position de chaque entier dans le tableau d entier compresse
            int pos = 0;
            
            // Declarer la valeur a repeter
            int valeur = 0;
            
            // Declarer le nombre de repetitions
            int repetitions = 0;       
        
            // Decompresser le tableau d entier
            for (int i = 0 ; i < taille ; i++){
            
                // Evaluer le nombre de repetitions
                if (repetitions == 0){
                
                    // Determiner la nouvelle valeur a repeter
                    valeur = t[pos++];
                    
                    // Determiner le nombre de repetitions
                    repetitions = t[pos++];   
                }
                
                // Verifier que le nombre de repetitions est positif
                if (repetitions > 0){
                    
                    // Copier la valeur a repeter dans le nouveau tableau
                    decompression[i] = valeur;
                    
                    // Decrementer le nombre de repetitions
                    repetitions--;
                }
            }
        }
        
        // Retourner le tableau decompresse
        return decompression;
    }
}



