// Classe Element
class Element {

    // Champs
    Occurrence occurrence;
    int suivant;

    // Constructeur
    Element (Occurrence e, int s) {
        occurrence = e;
        suivant = s;
    }
}

// Classe Occurence
class Occurrence {

    // Champs
    int retour;
    int taille;
    
    // Constructeur
    Occurrence (int retour, int taille) {
        this.retour = retour;
        this.taille = taille;
    }
    
    // Fonction affichant les valeurs de l occurence
    void affiche(){
        System.out.printf("(%d,%d)", retour, taille);
    }
}

// Classe LZ77
class LZ77 {

    // Constructeur
    LZ77(){
    }
   
    // Fonction determinant la plus longue occurence dans le tableau d entier
    public static Occurrence plusLongueOccurrence(int[] t, int positionCourante, int tailleFenetre) {
    
        // Verifier les parametres
        if (t == null){
            throw new IllegalArgumentException("Le tableau d entiers specifie en entree est nul");
        }
        if (positionCourante < 0){
            throw new IllegalArgumentException("La position courante est negative");
        } 
        if (tailleFenetre < 0){
            throw new IllegalArgumentException("La taille de la fenetre est negative");
        }
    
        // Declarer une occurrence
        Occurrence occurrence = new Occurrence(0, 0);
        
        // Declarer la borne inferieure de la fenetre
        int inf = positionCourante - tailleFenetre;
        
        // Mettre a jour la borne inferieur si la valeur est negative
        if (inf < 0){
            inf = 0;
        }
        
        // Parcourir toutes les valeurs de la fenetre
        for (int indiceFenetre = inf; indiceFenetre < positionCourante ; indiceFenetre++){
            
            // Declarer la taille de l occurence
            int tailleOccurrence = 0;
            boolean boucle = ((tailleOccurrence + indiceFenetre) < positionCourante) && ((tailleOccurrence + positionCourante) < t.length);
            while (boucle) {
        
                // Comparer les valeurs
                boolean ok = t[tailleOccurrence + indiceFenetre] == t[tailleOccurrence + positionCourante]; 
            
                // Incrementer l indice de l occurence
                if (ok){
                    tailleOccurrence++;
                }
            
                // Mettre a jour la variable de sortie de boucle
                boucle = ((tailleOccurrence + indiceFenetre) < positionCourante) && ((tailleOccurrence + positionCourante) < t.length) 
                                                                                 && (ok);
            }
                
            // Est ce que cette occurence a de meilleures proprietes ?
            if ( (tailleOccurrence > 0) && ( (tailleOccurrence > occurrence.taille) 
                                        || ( (tailleOccurrence == occurrence.taille) 
                                        && ( (positionCourante - indiceFenetre) > occurrence.retour) ) ) ) {
     
                // Mettre a jour l occurrence a retourner
                occurrence.taille = tailleOccurrence;
                occurrence.retour = positionCourante - indiceFenetre;
            }
        }
        
        // Retourner l occurence trouvee
        return occurrence;   
    }
    
    // Fonction retournant le nombre d'elements necessaire pour representer le tableau d entier au format LZ77
    public static int LZ77Longueur(int[] t, int tailleFenetre){
    
        // Verifier les parametres
        if (t == null){
            throw new IllegalArgumentException("Le tableau d entiers specifie en entree est nul");
        } else if (t.length == 0) {
            return 0;
        }
        if (tailleFenetre < 0){
            throw new IllegalArgumentException("La taille de la fenetre est negative");
        }
        
        // Declarer le nombre d elements
        int nbElements = 0;
        
        // Parcourir le tableau d entrees
        int positionCourante = 0;
        while (positionCourante < t.length){
        
            // Trouver la plus grande occurrence
            Occurrence occurence = plusLongueOccurrence(t, positionCourante, tailleFenetre);
            
            // Incrementer le nombre d elements
            nbElements++;
        
            // Avancer de k + 1 elements
            positionCourante += (occurence.taille + 1); 
        }
        
        // Retourne le nombre d elements
        return nbElements;
    }
    
    // Fonction retournant la version compressee du tableau d entiers
    public static Element[] LZ77(int[] tab, int tailleFenetre){
    
        // Verifier les parametres
        if (tab == null){
            throw new IllegalArgumentException("Le tableau d entiers specifie en entree est nul");
        } 
        if (tailleFenetre < 0){
            throw new IllegalArgumentException("La taille de la fenetre est negative");
        }
        
        // Determiner le nombre d elements
        int nbElements = LZ77Longueur(tab, tailleFenetre);
        
        // Declarer le tableau d elements
        Element[] elements = new Element[nbElements];
        
        // Verifier que le nombre d elements est positif
        if (nbElements > 0){
        
            // Parcourir le tableau d entrees
            int positionCourante = 0;
            int indice = 0;
            while (positionCourante < tab.length){
            
                // Trouver la plus grande occurrence
                Occurrence occurrence = plusLongueOccurrence(tab, positionCourante, tailleFenetre);
                
                // Determiner le caractere qui suit la plus grande occurence
                int caractere = tab[positionCourante + occurrence.taille];
                
                // Stocker l occurence et le caractere qui suit
                elements[indice] = new Element(occurrence, caractere);
                
                // Incrementer l indice du tableau d elements
                indice++;
            
                // Avancer de k + 1 elements
                positionCourante += (occurrence.taille + 1); 
            }
        }
        
        // Retourne le tableau d elements
        return elements;
    }
    
    // Fonction affichant le tableau d elements
    public static void afficheEncode(Element[] tab){
        
        // Verifier le parametre
        if (tab == null){
            throw new IllegalArgumentException("Le tableau d elements specifie en entree est nul");
        } 
    
        // Afficher chaque element
        for (Element e: tab) {
            e.occurrence.affiche();
            System.out.printf("%d", e.suivant);
        }
        
        // Afficher un retour chariot
        System.out.printf("\n");
    }
    
    // Fonction retournant la taille du tableau d entier apres decompression
    public static int LZ77InverseLongueur(Element[] t){
    
        // Verifier le parametre
        if (t == null){
            throw new IllegalArgumentException("Le tableau d elements specifie en entree est nul");
        } else if (t.length == 0) {
            return 0;
        }
        
        // Declarer une taille nulle
        int taille = 0;
        
        // Lire le tableau du 1er entier a l avant dernier
        for (int i = 0 ; i < t.length ; i++){
                
            // Mettre a jour la taille a partir de l occurence
            taille += t[i].occurrence.taille;
            
            // Incrementer la taille pour le caractere qui suit
            taille++;
        }
        
        // Retourner la taille
        return taille;
    }
    
    // Fonction copiant les caracteres du tableau t1 allant de start1 a start1 + len - 1 vers le tableau t2 a partir de la position start2
    static void blit(int[] t1, int[] t2, int start1, int len, int start2){
    
        // Verifier les parametres
        if (t1 == null){
            throw new IllegalArgumentException("Le tableau 1 d entiers specifie en entree est nul");
        }
        if (t2 == null){
            throw new IllegalArgumentException("Le tableau 2 d entiers specifie en entree est nul");
        }
        if (start1 < 0){
            throw new IllegalArgumentException("L indice du tableau 1 est negatif");
        }
        if (len < 0){
            throw new IllegalArgumentException("La taille du sous-tableau 1 a copier est negatif");
        }
        if (start2 < 0){
            throw new IllegalArgumentException("L indice du tableau 2 est negatif");
        }
        
        // Copier les caracteres du tableau t1 au tableau t2
        for (int i = 0 ; i < len ; i++){
            t2[start2 + i] = t1[start1 + i];
        }
    }
    
    // Fonction retournant la version decompressee du tableau d elements
    public static int[] LZ77Inverse(Element[] t){
    
        // Verifier le parametre
        if (t == null){
            throw new IllegalArgumentException("Le tableau d elements specifie en entree est nul");
        }
        
        // Determiner la taille du tableau a retourner
        int taille = LZ77InverseLongueur(t);
        
        // Declarer le tableau a retourner
        int[] decompression = new int[taille];
        
        // Declarer l indice pour le tableau de decompression
        int indice = 0;
        
        // Verifier que la taille est positive
        if (taille > 0){
        
            // Parcourir le tableau d elements
            for (int i = 0; i < t.length ; i++){
            
                // Determiner si l occurence est de taille positive
                if (t[i].occurrence.taille > 0){
                
                    // Copier l occurence dans le tableau de decompression
                    blit(decompression, decompression, indice - t[i].occurrence.retour, t[i].occurrence.taille, indice);
                    
                    // Ajouter taille a l indice
                    indice += t[i].occurrence.taille;
                }
                 
                // Copier le caractere qui suit l occurence dans le tableau de decompression
                decompression[indice] = t[i].suivant;
                    
                // Incrementer l indice
                indice++; 
            }
        }
        
        // Retourner le tableau d entiers
        return decompression;
    }
    
    // Fonction affichant le tableau d entier
    public static void afficheDecode(int[] t){
        
        // Verifier le parametre
        if (t == null){
            throw new IllegalArgumentException("Le tableau d entiers specifie en entree est nul");
        } 
        
        // Afficher chaque entier
        for (int i: t) {
            System.out.printf("%d ", i);
        }
        
        // Afficher un retour chariot
        System.out.printf("\n");
    }
}
