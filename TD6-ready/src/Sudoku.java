// Dependance
import java.util.Arrays;


// Classe Sudoku
class Sudoku {
	
  int[][] grille;
  int nbSolutions;
    
  
  // Constructeur
  Sudoku(int[][] t) {
		
		// Verifier le parametre
		if (t == null){
			throw new IllegalArgumentException("Le sudoku specifie en entree est nul");
		}
		if (t.length != 9){
			throw new IllegalArgumentException("Le tableau de sudoku n est pas de taille 9");
		}
		for (int i = 0 ; i < 9 ; i++){
			if (t[i].length != 9){
				throw new IllegalArgumentException("Le tableau de sudoku n est pas de taille 9");
			}
		}
		
		// Allocation de la grille
    grille = new int[9][9];
    
    // Copie du tableau en parametre
    for (int i = 0 ; i < 9 ; i++){
			for (int j = 0 ; j < 9 ; j++){
				grille[i][j] = t[i][j];
			}
		}
		
		// Initialiser le nombre de solutions
		nbSolutions = 0;
  }
  
  
  // Trier le tableau et comparer chaque case avec la suivante
  private boolean trieEtVerifieProchain(int[] t){
		
		// Verifier le parametre
		if (t == null){
			throw new IllegalArgumentException("Le tableau specifie en entree est nul");
		}
		if (t.length != 9){
			throw new IllegalArgumentException("Le tableau n est pas de taille 9");
		}
		
		// Trier le tableau
		Arrays.sort(t);
		
		// Parcourir le tableau
		for (int j = 0 ; j < 8 ; j++){
			
			// Verifier que la case n est pas vide
			if (t[j] != 0) {
				
				// Comparer l actuelle case avec la suivante
				if (t[j] == t[j+1]){
					return false;
				}
			}
		}
		
    // Pas de doublons (autre que 0)
    return true;
	}
  
  
  // Verifier que toutes les valeurs (autre 0) sur la meme ligne sont differentes
  public boolean verifieLigne(int i) {
		
		// Verifier le parametre
		if (i < 0){
			throw new IllegalArgumentException("La ligne specifiee en entree n est pas coherente");
		}
		if (i >= 9){
			throw new IllegalArgumentException("La ligne specifiee en entree n est pas coherente");
		}
		
		// Copier la ligne
		int ligne[] = new int[9];
		for (int j = 0; j < 9; j++){
			ligne[j] = grille[i][j];
		}
		
		// Doublons ?
		return trieEtVerifieProchain(ligne);
  }


	// Verifier que toutes les valeurs (autre 0) sur la meme ligne sont differentes
  public boolean verifieColonne(int i) {
		
		// Verifier le parametre
		if (i < 0){
			throw new IllegalArgumentException("La ligne specifiee en entree n est pas coherente");
		}
		if (i >= 9){
			throw new IllegalArgumentException("La ligne specifiee en entree n est pas coherente");
		}
		
		// Copier la colonne
		int colonne[] = new int[9];
		for (int j = 0; j < 9; j++){
			colonne[j] = grille[j][i];
		}
		
		// Doublons ?
		return trieEtVerifieProchain(colonne);
  }


	// Verifier que toutes les valeurs (autre 0) dans le meme carre sont differentes
  public boolean verifieCarre(int i,int j) {
		
		// Determine les limites du carre
		int	divLigne = i / 3;
		int divColonne = j / 3;
		
		// Copier le carre dans un tableau de taille 9
		int carre[] = new int[9];
		int indice = 0;
		for (int ligne = 3 * divLigne; ligne < 3 * divLigne + 3; ligne++){
			for (int colonne = 3 * divColonne; colonne < 3 * divColonne + 3; colonne++){
				carre[indice++] = grille[ligne][colonne];
			}
		}
		
    // Doublons ?
    return trieEtVerifieProchain(carre);
  }
  
  
  // Verifier si la modification de la case (i, j) est possible
  public boolean verifiePossible(int i, int j, int val) {
		
		// Verifier les parametre
		if (i < 0){
			throw new IllegalArgumentException("La ligne specifiee en entree n est pas coherente");
		}
		if (i >= 9){
			throw new IllegalArgumentException("La ligne specifiee en entree n est pas coherente");
		}
		if (j < 0){
			throw new IllegalArgumentException("La colonne specifiee en entree n est pas coherente");
		}
		if (j >= 9){
			throw new IllegalArgumentException("La colonne specifiee en entree n est pas coherente");
		}
		if (val == 0){
			throw new IllegalArgumentException("La valeur specifiee en entree n est pas coherente");
		}
		
		// Declarer le resultat a retourner
    boolean res = (grille[i][j] == 0);
    
    // Verifier que la case n est pas occupee
    if (res){
			
			// Ajouter la valeur dans le tableau
			grille[i][j] = val;
			
			// Verifier que l ajout est correct
			res = verifieLigne(i) && verifieColonne(j) && verifieCarre(i, j);
			
			// Remettre a 0 la valeur
			grille[i][j] = 0;
		}
		
		// Retourner le  resultat
		return res;
  }
  
  
  // Remplir la grille a partir de (i, j)
  public boolean resousGrille(int i, int j){
				
		// Avons nous parcouru toute la grille ?
		if ( (i == 8) && (j==9) ) {
			return true;
			
		}	else {
			
			// Mettre a jour les indices
			if (j == 9){
				j = 0;
				i++;
			}
		}
		
		// Determiner si la position est libre
		if (grille[i][j] == 0){
			
			// Determiner toutes les valeurs possibles
			for (int val = 1 ; val <= 9 ; val++){
				
				// Copier la grille
				Sudoku copie = new Sudoku (grille);
								
				// Verifier que l ajout de la valeur est possible
				if (verifiePossible(i, j, val)){
					
					// Mettre a jour la case
					grille[i][j] = val;
					
					// Poursuivre la resolution a partir de la prochaine case de la copie
					if (resousGrille(i, j + 1)) {
						return true;
					}
						
					// Restaurer la grille
					grille = copie.grille;
				}
			}
			
		} else {
			
			// Poursuivre la resolution a partir de la prochaine case de la copie
			if (resousGrille(i, j + 1)) {
				return true;
			}
		}
		
		// Pas de solution
		return false;
	}
  
  
  // Determiner si la grille a une solution
  public boolean resousGrille() {
		return resousGrille(0,0);
  }


  // Determiner le nombrer de solution et remplir la grille a partir de (i, j) 
  public boolean solutionUnique(int i, int j){
				
		// Avons nous parcouru toute la grille ?
		if ( (i == 8) && (j==9) ) {
			
			// Determiner le nombre actuel de solutions
			if (nbSolutions <= 1){
				
				// Incrémenter le nombre de solutions trouvees
				nbSolutions++;
			
				// Retourner false pour poursuivre la recursion
				return false;
				
			} else {
				
				// On a atteint 2 solutions donc plus besoin de poursuivre
				return true;
			}
			
		}	else {
			
			// Mettre a jour les indices
			if (j == 9){
				j = 0;
				i++;
			}
		}
		
		// Determiner si la position est libre
		if (grille[i][j] == 0){
			
			// Determiner toutes les valeurs possibles
			for (int val = 1 ; val <= 9 ; val++){
				
				// Copier la grille
				Sudoku copie = new Sudoku (grille);
								
				// Verifier que l ajout de la valeur est possible
				if (verifiePossible(i, j, val)){
					
					// Mettre a jour la case
					grille[i][j] = val;
					
					// Poursuivre la resolution a partir de la prochaine case de la copie
					if (solutionUnique(i, j + 1)) {
						return true;
					}
						
					// Restaurer la grille
					grille = copie.grille;
				}
			}
			
		} else {
			
			// Poursuivre la resolution a partir de la prochaine case de la copie
			if (solutionUnique(i, j + 1)) {
				return true;
			}
		}
		
		// Pas de solution
		return false;
	}

	
	// Determiner s il existe plusieurs solutions
  public int solutionUnique() {
		solutionUnique(0,0);
		return nbSolutions;
  }
  
  
  // Afficher la grille du sudoku
  public void afficheGrille() {
    if (this.grille == null)
      return;

    for(int i = 0 ;i < 9; i++) {
      for(int j = 0; j < 9; j++) {
        System.out.print(this.grille[i][j]+" ");
      }
      System.out.println();
    }
  }
}
