// Classe TableDeHachage 
public class TableDeHachage {
	Liste[] listes;

	// Constructeur
  public TableDeHachage (int n) {
  	
  	// Verifier que le parametre est positif
  	if (n > 0){
			listes = new Liste [n];
			
			// Allouer dynamiquement chaque liste
			for (int i = 0 ; i < n ; i++){
				listes[i] = new Liste();
			}
		}
	}
	
	int determinerIndex(Objet o){
		int modulo = o.hash() % listes.length;
		if (modulo < 0){
			return modulo + listes.length;
		} else {
			return modulo;
		}
	}
	
	// Fonction ajoutant un objet dans la table de hachage
	public void ajoute(Objet o){
	
		// Determiner l index dans la table de hash
		int index = determinerIndex(o);
		
		// Ajouter dans la liste correspondant au hash
		listes[index].ajouteTete(o);
	}
	
	// Fonction verifiant que la table contient un objet
	public boolean contient(Objet o){
		
		// Determiner l index dans la table de hash
		int index = determinerIndex(o);
	
		// Verifier que la liste correspondant au hash contient l objet
		return listes[index].contient(o);
	}
	
	// Fonction renvoyant l indice de la liste la plus remplie ainsi que le nombre d objets qu elle contien
	public int[] remplissageMax(){
	
		// Declarer le resultat a retourner
		int[] resultat = new int[2];
		
		// Parcourir toutes les listes
		for (int i = 0 ; i < listes.length ; i++){
			int longueur = listes[i].longueur();
			if (resultat[1] < longueur){
				resultat[0] = i;
				resultat[1] = longueur;
			}
		}
		
		// Retourner le resultat
		return resultat;
	}
}
