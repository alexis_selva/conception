// Classe Cellule 
class Cellule {
	Objet valeur;
	Cellule suivante;
          
  // Constructeur
  public Cellule (Objet valeur, Cellule suivante) {
  	this.valeur = valeur;
  	this.suivante = suivante;
	}
}

// Classe Liste 
public class Liste {
	private Cellule tete;
          
  // Constructeur
  public Liste () {
  	tete = null;
	}
	
	// Fonction ajoutant une cellule en tete
	public Liste ajouteTete(Objet val){
		
		// Ajouter en tete une cellule
		tete = new Cellule (val, tete);
		
		// Retour la liste
		return this;
	}

	// Fonction supprimant la cellule en tete
	public Liste supprimeTete() {
	
		// Verifier que la liste n est pas vide
		if (tete == null){
			throw new java.util.NoSuchElementException("Impossible de supprimer la tete d une liste vide");
		}
		
		// Supprimer la 1ere cellule
		tete = tete.suivante;
		
		// Retour la liste
		return this;
	}
	
	// Fonction verifiant si la liste contient un objet du meme nom
	public boolean contient(Objet o){
	
		// Declarer la cellule courante
		Cellule courante = tete;
		
		// Parcourir les cellules jusqu a trouver celle qu on chercher
		while (courante != null){
		
			// Verifier que si la cellule courant contient un objet du meme nom
			if (courante.valeur.nom().equals(o.nom())){
				return true;
			}
			
			// Mettre a jour la cellule courante
			courante = courante.suivante;
		}
		
		// Si on est la, cela signifie que ce qu on cherche n a pas ete trouve
		return false;
	}
	
	// Fonction retournant la longueur de la liste
	public int longueur(){
	
		// Declarer le resultat
		int resultat = 0;
		
		// Declarer la cellule courante
		Cellule courante = tete;
		
		// Parcourir les cellules jusqu a trouver celle qu on chercher
		while (courante != null){
		
			// Incrementer le resultat
			resultat++;
			
			// Mettre a jour la cellule courante
			courante = courante.suivante;
		}
		
		// Retourner le resultat
		return resultat;
	}
}
