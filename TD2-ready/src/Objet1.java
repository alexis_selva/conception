public class Objet1 extends Objet {
  String nom;
  
  // Constructeur
  Objet1(String n) {
    nom = n;
  }
  
  // Fonction calculant le hash de nom
  public int hash() {
  	int resultat = 0;
  	
  	// Parcourir les caracteres du nom
  	for (int i = 0 ; i < nom.length() ; i++){
  	
  		// Calculer le hash pour chaque caractere
  		resultat += nom.charAt(i) * calculPuissance(31, nom.length() - 1 - i); 
  	}

		// Retourner le resulat
		return resultat;
  }

  // Fonction retournant le nom
  public String nom() {
    return nom;
  }
  
  // Fonction calculant la puissance (positive) d un entier
  static int calculPuissance (int value, int puissance){
  	if (puissance == 0){
  		return 1;
  	} else {
  		return value * calculPuissance(value, puissance -1);
  	}
  }
}
