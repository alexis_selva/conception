public class Objet2 extends Objet {
  String nom;
  
  // Constructeur
  Objet2(String n) {
    nom = n;
  }
  
  // Fonction calculant le hash de nom
  public int hash() {
  	int resultat = 5381;
  	
  	// Parcourir les caracteres du nom
  	for (int i = 0 ; i < nom.length() ; i++){
  	
  		// Calculer le hash pour chaque caractere
  		resultat = (resultat * 33) ^ nom.charAt(i); 
  	}

		// Retourner le resulat
		return resultat;
  }

  // Fonction retournant le nom
  public String nom() {
    return nom;
  }
}
