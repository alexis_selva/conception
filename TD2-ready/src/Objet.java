/* Ceci est la classe des objets possedant une fonction de hash. Elle est
 * abstraite : a vous d ecrire des fichiers Objet1.java et Objet2.java pour
 * implementer vos propres versions ! */
public abstract class Objet {
  abstract int hash();
  abstract String nom();
}
