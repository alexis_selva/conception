# Devoir 4 : Recherche de mots dans une grille - partie 2

Présentation du devoir
Nous allons cette semaine expliquer comment résoudre de manière efficace le jeu présenté en introduction du cours. Il s'agissait, pour mémoire, d'explorer une grille de lettres, pour y trouver tous les mots valides en se déplaçant horizontalement, verticalement, ou en diagonale.

Explorer tous les chemins possibles dans la grille serait pure folie : il y en a tout simplement beaucoup trop. En revanche, en associant chaque déplacement sur la grille à un déplacement dans l'arbre préfixe correspondant, nous nous rendrons compte très vite que la plupart des chemins sur la grille ne correspondent pas à des préfixes des mots du dictionnaire. Ainsi, nous n'aurons que peu de chemins valides à explorer.

Nous aurons donc besoin des arbres préfixes codés la semaine dernière : ce sujet commence là où celui de la semaine dernière s'arrête.

Obtenir le sujet / soumission des résultats
L'archive est disponible en ligne. Téléchargez-la, puis extrayez son contenu dans le dossier de votre choix.

La procédure de soumission suit le même protocole que pour le devoir précédent : des fonctions de test à décommenter et à ne surtout pas modifier, et la sortie de votre programme qui sera validée ou non par Coursera.

En cas de problème
Si vous rencontrez des soucis techniques avec le processus de soumission, ou si le sujet vous semble ambigu ou contenir une erreur, les forums du cours sont à votre disposition pour demander de l'aide.

Rappels de Java
Nous rappelons que pour itérer sur une structure de données en java, on peut utiliser la syntaxe suivante :

LinkedList<T> l = ...;
for (T x: l) {
  // faire quelque chose avec x
  }
  Nous rappelons également deux instructions de contrôle de boucle : dans le corps d'une boucle, l'instruction continue permet de passer directement à l'itération suivante d'une boucle sans exécuter le reste du corps de la boucle ; l'instruction break permet de terminer la boucle prématurément.
  
  Données de l'exercice
  Cet exercice fait intervenir un dictionnaire des mots du français. Le script java, lorsqu'il est lancé, s'attend à être lancé depuis un dossier courant contenant le fichier dico.txt, fourni dans l'archive. Si vous utilisez un IDE, vous devez impérativement apprendre à la configurer pour être certain que votre programme s'exécutera dans le bon working directory (dossier courant). Vous aurez un message d'erreur utile si ça n'est pas le cas. Dans le cas de Netbeans, le fichier dico.txt doit être placé à la racine du répertoire projet.
  
  Description de l'algorithme ; extension des arbres préfixes
  Importez dans vos sources le fichier Dictionnaire.java que vous avez écrit la semaine dernière. Regardez la vidéo d'introduction du cours et assurez-vous que le fonctionnement du jeu est clair pour vous.
  
  Fonctionnement de l'algorithme
  Notre algorithme sera amené à parcourir des chemins sur la grille. À chaque chemin sera associé un préfixe.
  
  Le fonctionnement de l'algorithme est intuitif. Illustrons-le à l'aide d'une grille d'exemple.
  
  l
  
  e
  
  a
  
  s
  
  Imaginons que nous parcourons la grille ci-dessus. Nous pouvons commencer de la parcourir à la lettre a.
  
  l
  
  e
  
  a
  
  s
  
  La question se pose alors : est-ce que a est un préfixe valide ? En d'autres termes, existe-t-il des mots qui commencent par la lettre a ? Si ça n'est pas le cas, autant arrêter de suite ! Fort heureusement, la réponse est oui. Le chemin constitué de la seule case inférieure gauche est donc valide ; son préfixe associé est a.
  
  Profitons-en pour nous demander si a est un mot valide : la réponse est oui également (« il a fini son devoir »). Nous avons trouvé notre premier mot.
  
  Les déplacements valides pour ce jeu sont les déplacements d'une case en diagonale, à l'horizontale, ou à la verticale. Nous pouvons donc nous déplacer ou bien vers la lettre l, la lettre e, ou la lettre s. Déplaçons-nous vers la lettre s. Nous explorerons les autres choix plus tard.
  
  l
  
  e
  
  a→
  
  s
  
  Le préfixe as est-il un préfixe valide ? La réponse est oui. Le chemin constitué de la case a puis de la case s, associé au préfixe as, est donc valide. Il se trouve également que as est un mot valide (« as de pique »). Nous avons trouvé notre deuxième mot.
  
  Déplaçons-nous vers la lettre e. Nous verrons ce que donne un déplacement vers la lettre l plus tard.
  
  l
  
  e
  
  a→
  
  s↑
  
  Le préfixe ase est un préfixe valide (« aseptiser »), mais ne constitue pas un mot. Le seul déplacement autorisé reste alors la lettre l.
  
  l
  
  ←e
  
  a→
  
  s↑
  
  asel n'est pas un mot : nous abandonnons de suite.
  
  Ici, nous n'avons absolument pas terminé. Il nous faut en effet revenir en arrière.
  
  l
  
  e
  
  a→
  
  s↑
  
  Lorsque nous étions à l'étape ci-dessus, y avait-il d'autres possibilités de déplacement sur la grille que nous n'aurions pas traitées ? La lettre l était notre seul choix. Revenons d'encore un pas en arrière.
  
  l
  
  e
  
  a→
  
  s
  
  Ici, nous avons exploré un déplacement vers la lettre e, mais pas vers la lettre l. Il est temps de le faire.
  
  l
  
  e
  
  a→
  
  ↖s
  
  asl n'est pas un préfixe valide : nous nous rendons compte qu'il n'est pas la peine de pousser plus loin l'exploration. En effet, l'arbre préfixe nous informe que nous ne trouverons aucune solution qui commence par asl. Nous abandonnons donc cette voie, et n'explorons pas de chemins plus longs.
  
  On voit ici l'intérêt de l'arbre préfixe : sans arbre préfixe, nous aurions envisagé bon nombre de chemins inutiles.
  
  Revenons d'un pas en arrière.
  
  l
  
  e
  
  a→
  
  s
  
  Nous avons exploré e et l : il ne nous reste plus rien à effectuer ici. Revenons d'encore un pas en arrière.
  
  l
  
  e
  
  a
  
  s
  
  Ici, nous avons exploré uniquement à partir de la lettre s. Il faut encore explorer la lettre l (nous trouvons ale et ales, un style de bière) et la lettre e (nous ne trouvons rien).
  
  Si l'on remonte encore d'un cran, il nous faut encore envisager les mots qui commencent en s (nous trouvons se, sel, sa, sale), en e (nous trouvons es) et en l (nous trouvons le, les, lesa, la, las).
  
  Utilisation efficace de l'arbre préfixe
  À chaque étape de l'algorithme, nous utilisons l'arbre préfixe, pour savoir si une séquence de caractères constitue un préfixe valide.
  
  La méthode dédiée, que vous avez implémentée pour le devoir précédent, s'appelle public boolean estPrefixe(String m). Cette méthode parcourt tous les nœuds de l'arbre jusqu'à déterminer si la chaîne passée en argument constitue un préfixe valide.
  
  Nous avons ici un problème de complexité algorithmique. Appeler estPrefixe à chaque étape de l'algorithme est inefficace : la complexité est quadratique par rapport au nombre de nœuds visités. En effet, notre algorithme a d'abord demandé si ale était un préfixe, puis si ales était un préfixe. On vérifié deux fois que ale était un préfixe. Il aurait été plus efficace de se souvenir que ale était déjà un préfixe, puis de demander : à partir de ale, peut-on obtenir le préfixe ales ?
  
  Notre algorithme va donc maintenir un pointeur vers un nœud de l'arbre : au moment de se déplacer dans la grille, l'algorithme se déplacera également dans l'arbre préfixe. Au moment de faire un pas en arrière, notre algorithme reviendra d'une case en arrière dans la grille, et remontera d'un nœud dans l'arbre préfixe. En d'autres termes, nous allons effectuer un parcours simultané de l'arbre préfixe et de la grille.
  
  Méthodes supplémentaires pour les Noeuds
  Pour ce faire, nous aurons besoin de méthodes supplémentaires pour les nœuds. Créez la méthode public Noeud trouveFils(char c) qui prend un caractère, et retourne le nœud parmi les enfants correspondant à ce caractère s'il existe, ou null sinon.
  
  Une fois cette fonction définie, réutilisez là pour implémenter public boolean estMot() qui vous dit si le nœud marque la fin d'un mot, ou non.
  
  Un petit test vous est fourni pour vérifier que votre fonction fait bien ce qui est demandé. Activez test5 et soumettez la partie correspondante.
  
  Présentation de l'algorithme et squelette de code
  Voyons maintenant un peu plus en détail comment implémenter cet algorithme.
  
  Spécification de l'algorithme
  Nous aurons une grille, de forme carrée. Les déplacements autorisés sont expliqués dans la vidéo : horizontalement, verticalement, en diagonale. Il est bien évidemment interdit de passer deux fois sur une même case ; tous les déplacements se feront d'au plus une case sur chaque axe.
  
  L'algorithme maintiendra un état, c'est-à-dire un ensemble de variables qui représentent la progression de l'algorithme. Un état sera constitué des variables suivantes :
  
  un chemin courant (implicite) : l'ensemble des cases parcourues précédemment ;
  un préfixe courant, c'est-à-dire l'ensemble des caractères trouvés sur le chemin courant (en fond rouge sur les exemples) ;
  un masque courant, c'est-à-dire une grille de booléens, qui permet de savoir si une case a été visitée ou pas,
  une liste de mots trouvés.
  L'algorithme s'appellera récursivement, en prenant les paramètres suivants :
  
  un nœud courant, c'est-à-dire un nœud de l'arbre préfixe correspondant au préfixe courant ;
  une position désirée, c'est-à-dire une case de la grille, représentée par ses coordonnées (lettre entourée en rouge sur les exemples).
  Voici un état de l'exploration, tiré de l'exemple vu précédemment.
  
  l
  
  e
  
  a→
  
  s↑
  
  L'arbre préfixe correspondant est le suivant :
  
          +---+
                  | a |
                          +---+
                               ↙    ↓    ↘ 
                               +---+   +---+   +---+  
                               | * | … | s | … | z |
                               +---+   +---+   +---+
                                    ↙    ↓    ↘      ↘  
                                    +---+   +---+   +---+   …
                                    | * | … | e | … | y |
                                    +---+   +---+   +---+
                                    Voici l'état de l'algorithme :
                                    
                                    Le préfixe courant est as.
                                    Le nœud courant est le nœud s de l'arbre préfixe.
                                    Le masque courant est :
                                    false
                                    
                                    false
                                    
                                    true
                                    
                                    true
                                    
                                    La position désirée est le e.
                                    Les mots trouvés sont a et as.
                                    Attention, c'est bien vers le nœud s de l'arbre préfixe que nous maintenons un pointeur : ce pointeur correspond à la fin du chemin parcouru. La lettre e constitute la position souhaitée : nous ne l'avons pas encore visitée !
                                    
                                    En effet, si l'algorithme désire aller à la position p, il faut :
                                    
                                    vérifier si ajouter le caractère c à la position p au préfixe courant est possible ; si oui :
                                    ajouter c au préfixe courant, et
                                    mettre à jour le nœud courant pour pointer vers le caractère c, et
                                    se souvenir que nous nous sommes déplacés vers la case p en modifiant le masque ;
                                    vérifier si le nouveau nœud courant marque la fin d'un mot et, le cas échéant, étendre la liste de mots trouvés ;
                                    faire la liste des déplacements autorisés à partir de la position p, et s'appeler récursivement pour chacune des positions trouvées ;
                                    restaurer le masque et le préfixe.
                                    Nous allons implémenter pas à pas cet algorithme.
                                    
                                    Classes Exploration et Position
                                    Créez le fichier Exploration.java et créez les deux classes suivantes.
                                    
                                    Classe Exploration
                                    
                                    Nous allons créer une classe Exploration ; une instance de cette classe sera créée pour chaque grille que nous souhaiterons explorer.
                                    
                                    La classe Exploration contiendra des données immutables, qui sont les données de l'algorithme. Ces données ne changeront pas au cours de l'exploration. Ajoutez ainsi les variables membres suivantes :
                                    
                                      final char[][] grille;
                                        final int dim;
                                          final Dictionnaire d;
                                          Le mot-clé final indique que ces variables seront initialisées dans le constructeur, et ne seront plus modifiées par la suite. L'entier dim représente la dimension de la grille (carrée) et le dictionnaire d vous sera fourni. La grille est représentée par un tableau de char. Implémentez le constructeur Exploration (char[][] grille, int dim, Dictionnaire d).
                                          
                                          Une instance de la classe exploration va représenter l'état dont nous avons parlé précédemment. Ajoutez donc les variables membres suivantes à votre classe. Ces variables seront modifiées au fur et à mesure que l'exploration progresse.
                                          
                                            // Variables représentant l'état de l'exploration, modifiées par les fonctions
                                              // "explore" et "exploreTout". Elles seront initialisées par la fonction
                                                // "exploreTout`"
                                                  boolean[][] masque;
                                                    LinkedList<Character> prefix;
                                                      LinkedList<String> motsTrouves;
                                                      Classe Position
                                                      
                                                      La classe Position représente une position dans la grille. La position aura besoin de faire référence aux données de l'Exploration en cours. Vous devrez donc implémenter un constructeur public Position(Exploration e, int x, int y). De même, la classe Position devra stocker ces trois paramètres dans des variables membres.
                                                      
                                                      class Position {
                                                        final int x, y;
                                                          final Exploration e;
                                                            ...
                                                            Recherche des déplacements autorisés
                                                            Modifiez la classe Position, et ajoutez la méthode private boolean estLegal(). Une position est légale si elle se situe dans la grille et si la case n'a pas déjà été visitée.
                                                            
                                                            Ajoutez ensuite la méthode public List<Position> deplacementsLegaux() à la classe. Appeler p.deplacementsLegaux() renvoie une liste de positions p', tel que chaque p' est un déplacement légal accessible à partir de p.
                                                            
                                                            Un test très simple vous est fourni pour vous aider à vérifier votre code. Activez test6 et soumettez la partie correspondante.
                                                            
                                                            Implémentation de la recherche
                                                            Échauffement
                                                            Nous allons commencer par implémenter le point d'entrée de l'algorithme : la méthode public LinkedList<String> exploreTout(). Vous aurez peut-être besoin de documentation pour la classe LinkedList pour cette partie.
                                                            
                                                            Méthode explore1
                                                            
                                                            Commencez par implémenter la methode public void explore1(Position p, Noeud n). Cette méthode est une version simplifiée de la future méthode explore que nous écrirons dans peu de temps : elle n'explore qu'un seul caractère, et ne s'appelle pas récursivement.
                                                            
                                                            Cette fonction prend une Position désirée p, et le Noeud courant n. Cette fonction doit :
                                                            
                                                            prendre le caractère c à la position p ;
                                                            vérifier si ajouter c au préfixe courant this.prefix est possible ; si oui :
                                                            ajouter c au préfixe courant this.prefix, et
                                                            se souvenir que nous nous sommes déplacés vers la case p en modifiant le masque ;
                                                            vérifier si le nouveau nœud courant marque la fin d'un mot et, le cas échéant, étendre la liste de mots trouvés.
                                                            Pour vérifier si ajouter c au préfixe courant est possible, il suffit de faire appel à la méthode trouveFils du nœud courant n : si trouveFils renvoie null, alors il n'est pas possible de continuer le parcours, et il faut abandonner. Si trouveFils renvoie un nouveau nœud, alors on étend le préfixe.
                                                            
                                                            Une fois le nouveau nœud obtenu, vous pourrez utiliser la nouvelle méthode estMot() pour déterminer si le nœud correspond à un mot. Si c'est le cas, vous devrez ajouter le préfixe à la liste des mots trouvés.
                                                            
                                                            Vous aurez besoin de la fonction suivante pour convertir le préfixe qui est une LinkedList<Character> en String, avant de l'ajouter à this.motsTrouves.
                                                            
                                                              public static String versChaine(LinkedList<Character> l) {
                                                                  StringBuilder sb = new StringBuilder();
                                                                      for (Character c: l)
                                                                            sb.append(c);
                                                                                return sb.toString();
                                                                                  }
                                                                                  Méthode exploreTout
                                                                                  
                                                                                  Une fois explore1 écrite, écrivez public LinkedList<String> exploreTout(). Cette méthode commence par initialiser les trois variables d'état masque, motsTrouves et prefix mentionnées précédemment, puis parcourt toutes les cases de la grille ; pour chacune des cases, elle lance une exploration à partir de cette case à l'aide de la méthode explore1.
                                                                                  
                                                                                  Une fois les explorations terminées, la fonction renvoie la variable membre this.motsTrouves.
                                                                                  
                                                                                  Nous fournissons une fonction de test sur la grille suivante :
                                                                                  
                                                                                  l
                                                                                  
                                                                                  y
                                                                                  
                                                                                  a
                                                                                  
                                                                                  s
                                                                                  
                                                                                  Les seuls mots à une lettre contenus dans cette grille sont y et a (« Il y a... »). Votre fonction test7() doit afficher :
                                                                                  
                                                                                  test7 [y, a] end
                                                                                  ou bien :
                                                                                  
                                                                                  test7 [a, y] end
                                                                                  selon l'ordre dans lequel vous présentez la liste des déplacements possibles.
                                                                                  
                                                                                  Pas la peine de soumettre test7 pour l'instant, nous allons améliorer progressivement la sortie produite par cette fonction jusqu'au résultat final. Vous pourrez alors soumettre vos résultats.
                                                                                  
                                                                                  Backtracking
                                                                                  
                                                                                  Vous avez peut-être des mots de plus d'une lettre qui s'affichent dans le test ci-dessus. Si c'est le cas, alors vous avez un problème de backtracking. En effet, la variable prefix est partagée : c'est la même liste qui est utilisée par tous les appels à la fonction explore1. Vous devez donc écrire explore1 de manière à garantir qu'elle laisse la variable membre prefix dans l'état où elle l'a trouvé.
                                                                                  
                                                                                  Affichez le masque la fin de la méthode exploreTout (un simple System.out.println devrait donner un affichage lisible). Si votre masque n'est pas revenu dans l'état initial (toutes les cases à false), vous avez le même problème. Modifiez explore1 en conséquence.
                                                                                  
                                                                                  Exploration générale
                                                                                  Écrivez maintenant la méthode public void explore(Position p, Noeud n). Son code est identique à la méthode explore1, si ce n'est qu'elle parcourt la liste des déplacements p' possibles à partir de p, et s'appelle récursivement avec chaque déplacement p' et le nouveau nœud courant.
                                                                                  
                                                                                  Modifiez exploreTout pour qu'elle utilise explore et non plus explore1.
                                                                                  
                                                                                  Votre fonction test7 doit maintenant vous donner quelque chose comme (l'ordre, encore une fois, dépendra de vos choix d'exploration) :
                                                                                  
                                                                                  test7 [lys, lysa, la, las, y, a, as, sa] end
                                                                                  Ça n'est toujours pas nécessaire de soumettre la fonction test7.
                                                                                  
                                                                                  Tri des résultats de l'exploration
                                                                                  Nous vous demandons, pour finir, de trier les résultats de l'exploration à l'aide de l'ordre naturel sur le couple (longueur du mot, mot). En d'autres termes, votre fonction test7 doit maintenant donner :
                                                                                  
                                                                                  test7 [a, y, as, la, sa, las, lys, lysa] end
                                                                                  Vous pouvez maintenant soumettre test7.
                                                                                  
                                                                                  Les fonctions test8 jusqu'à test11 testent votre code sur d'autres grilles de test. Soumettez leurs résultats pour avoir les points. Attention : si un mot est présent plusieurs fois, il doit apparaître plusieurs fois dans votre sortie. La grille suivante doit donner [a, a, a, a].
                                                                                  
                                                                                  Ces fonctions utilisent des grilles plus larges. Si vous avez des problèmes de performance, vous avez certainement un problème de complexité algorithmique.
                                                                                  
                                                                                  a
                                                                                  
                                                                                  a
                                                                                  
                                                                                  a
                                                                                  
                                                                                  a
                                                                                  
                                                                                  Pour avoir les points, les résultats devront être triés de la même manière que pour test7.
