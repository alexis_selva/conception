// Import class
import java.io.*;
import java.util.*;

// Classe Noeud 
class Noeud {
	char valeur;
	LinkedList<Noeud> fils;
          
  // Constructeur
  public Noeud (char valeur) {
  	this.valeur = valeur;
  	fils = new LinkedList<Noeud>();
	}
	
	// Ajouter un fils a un noeud de tel facon a ce que l ordre alphabetique est assure
	public void ajouteFils(Noeud a){
		
		// Chercher la position ou ajouter le noeud
		int pos = 0;
		for (Noeud f : fils){
			
			// Verifier qu il ne s agit pas du caractere '*'
			if (f.valeur != '*'){
			
				// Verifier si le noeud doit etre ajoute a la position courante
				if (f.valeur > a.valeur){
					fils.add(pos, a);
					
					// Sortir de la fonction
					return;
				} 
			}
			
			// Incrementer la position
			pos++;
		}
		
		// Si la position n a pas ete trouvee, ajouter le fils en fin de liste
		fils.addLast(a);
	}
	
	// Afficher le noeud
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(valeur);
		
		// Y a t'il au moins un fils ?
		if (!fils.isEmpty()){
			sb.append('(');
			for (Noeud f : fils){
				sb.append(f.toString());
				
				// Est ce le dernier fils ?
				if (f != fils.getLast()){
					sb.append(", ");
				}
			}
			sb.append(')');
		}
		
		// Retour un string
		return sb.toString();
	}
	
	// Verifier si la lettre a la position pos du mot s est un fils de ce noeud
	public boolean existeMotRecursif(String s, int pos){
				
		// Parcourir tous les fils a la recherche d une lettre
		for (Noeud f : fils){
			
			// Si on atteint la fin du string, il faut chercher le caractere '*'
			if (pos >= s.length()){
								
				// Est ce la lettre de fin ?
				if (f.valeur == '*'){
					return true;
				}
				
			} else {
							
				// Est ce la bonne lettre ?
				if (f.valeur == s.charAt(pos)){
					
					// Verifier la prochaine lettre
					return f.existeMotRecursif(s, pos + 1);
				}
			}
		} 
		
		// Pas de lettre correspondante parmi les fils
		return false;
	}
	
	// Ajouter une lettre au noeud courant
	public boolean ajouteMotRecursif(String s, int pos){
			
		// Parcourir tous les fils a la recherche d une lettre
		for (Noeud f : fils){
						
			// Si on atteint la fin du string, il faut chercher le caractere '*'
			if (pos >= s.length()){
								
				// Est ce que le mot etait deja present dans les fils du noeud ?
				if (f.valeur == '*'){
					return false;
				}
				
			} else {
							
				// Est ce la bonne lettre ?
				if (f.valeur == s.charAt(pos)){
					
					// Verifier la prochaine lettre
					return f.ajouteMotRecursif(s, pos + 1);
				}
			}
		} 
		
		// Verifier qu on n a pas atteint la fin du string
		if (pos < s.length()){
			
			// Creer un nouveau noeud a partir d un caractere non ajoute de s
			Noeud n = new Noeud (s.charAt(pos));
			
			// Ajouter ce nouveau fils au noeud courant
			ajouteFils(n);
			
			// Poursuivre la creation de noeud
			return n.ajouteMotRecursif(s, pos + 1);
			
		} else {
			
			// Creer un nouveau noeud a partir du caractere '*'
			Noeud n = new Noeud ('*');
			
			// Ajouter ce nouveau fils au noeud courant
			ajouteFils(n);
			
			// Confirmer l ajout
			return true;
		}
	}
	
	// Verifier si le noeud courant fait partie du prefixe
	public boolean estPrefixeRecursif(String s, int pos){
					
		// Parcourir tous les fils a la recherche d une lettre
		for (Noeud f : fils){
						
			// Si on atteint la fin du string, c est qu un prefixe existe
			if (pos >= s.length()){
				return true;
				
			} else {
											
				// Est ce la bonne lettre ?
				if (f.valeur == s.charAt(pos)){
										
					// Verifier la prochaine lettre
					return f.estPrefixeRecursif(s, pos + 1);
				}
			}
		}
		
		// Si aucune lettre ne correspond, c est qu un prefixe n existe pas
		return false; 
	}
	
	// Convertir la liste de noeuds en string
	public String convertirMots(LinkedList<Character> prefix) {
		StringBuilder sb = new StringBuilder();
		for (char l : prefix){
			sb.append(l);
		} 
		return sb.toString();
	}
	
	// Lister les mots par ordre alphabetique
	public void listeMotsAlphabetique(LinkedList<Character> prefix) {
		
		// Est ce la fin d un mot ?
		if (valeur == '*'){
			System.out.printf("%s ", convertirMots(prefix));
			
		} else {
			
			// Ajouter en fin de file le caractere du noeud courant
			prefix.addLast(valeur);
			
			// Parcourir tous les fils
			for (Noeud f : fils){
				
				// Creer une liste temporaire
				LinkedList<Character> tmpPrefix = new LinkedList<Character> (prefix);
				
				// Appliquer recursivement la fonction sur les fils du noeud courant
				f.listeMotsAlphabetique(tmpPrefix);
			}
		}
	}
	
	// Trouver le fils qui commence par le caractere en entree
	public Noeud trouveFils(char c){
		
		// Parcourir les fils
		for (Noeud f : fils){
			
			// Verifier s il s agit du caractere en entree
			if (f.valeur == c){
				return f;
			}
		}
		
		// Pas de fils
		return null;
	}
	
		
	// Verifier si un noeud marque la fin d un mot
	public boolean estMot(){
		return (trouveFils('*') != null);
	}
}


// Classe Dictionnaire 
class Dictionnaire {
	Noeud racine;
          
  // Constructeur
  public Dictionnaire () {
  	racine = new Noeud ('_');
	}
	
	// Existe t'il un mot dans le dictionnaire ?
	public boolean existeMot(String s){ 
		
		// Le mot est il vide ?
		if (s.length() > 0){
			return racine.existeMotRecursif(s, 0);
		} else {
			return false;
		}
	}
	
	// Afficher le dictionnaire
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Noeud f : racine.fils){
			sb.append(f.toString());
			sb.append('\n');
		}
		return sb.toString();
	}
	
	// Ajouter un mot dans le dictionnaire
	public boolean ajouteMot(String s){
		
		// Le mot est il vide 
		if (s.length() > 0){
			return racine.ajouteMotRecursif(s, 0);
		} else {
			return false;
		}
	}
	
	// Verifier si le mot s est prefixe d un mot du dictionnaire
	public boolean estPrefixe(String s){
		
		// Le mot est il vide 
		if (s.length() > 0){
			return racine.estPrefixeRecursif(s, 0);
		} else {
			return false;
		}	
	}
	
	// Afficher tous les mots du dictionnaire par ordre alphabetique
	public void listeMotsAlphabetique(){
		for (Noeud f : racine.fils){
			
			// Allouer une liste vide
			LinkedList<Character> prefix = new LinkedList<Character>();
			
			// Lister les mots en partant de chaque fils
			f.listeMotsAlphabetique(prefix);
		}
	}
}
