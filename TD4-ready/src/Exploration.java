// Import class
import java.io.*;
import java.util.*;

// Classe Exploration 
class Exploration {
	
	// Variables immutables
	final char[][] grille;
  final int dim;
  final Dictionnaire d;
  
  // Variables representant l etat de l exploration, modifiees par les fonctions
  // "explore" et "exploreTout". Elles seront initialisees par la fonction
  // "exploreTout`"
  boolean[][] masque;
  LinkedList<Character> prefix;
  ArrayList<String> motsTrouves;
  
  // Constructeur
  Exploration (char[][] grille, int dim, Dictionnaire d){
		this.grille = grille;
		this.dim = dim;
		this.d = d;
	}
	
	// Convertir liste chaine de caracteres en string
	public static String versChaine(LinkedList<Character> l) {
    StringBuilder sb = new StringBuilder();
    for (Character c: l)
      sb.append(c);
    return sb.toString();
  }
	
	// Explorer un caractere dans la grille
	public void explore1(Position p, Noeud n){
		
		// Sauvegarder le prefixe
		LinkedList<Character> savePrefix = new LinkedList<Character> (prefix);
		
		// Sauvegarder le masque
		boolean[][] saveMasque = new boolean [dim][dim];
				for (int i = 0 ; i < dim ; i++){
			for (int j = 0 ; j < dim ; j++){
				saveMasque[i][j] = masque[i][j];
			}
		}
		
		// Prendre le caractere c a la position
		char c = p.e.grille[p.x][p.y];
		
		// Verifier que c peut etre ajoute au prefix
		Noeud nouveau = n.trouveFils(c);
		if (nouveau != null){
			
			// Ajouter c au prefixe courant
			prefix.add(c);
			
			// Modifier le masque
			masque[p.x][p.y] = true;
		
			// Verifier que le nouveau noeud courant marque la fin d un mot
			if (nouveau.estMot()){
				
				// Convertir le courant prefix en chaine
				String mot = versChaine(prefix);
				
				// Ajouter le mot a la liste des mots trouvees
				motsTrouves.add(mot);
			}
		}
		
		// Restaurer le prefixe
		prefix = savePrefix;
		
		// Restaurer le masque
		masque = saveMasque;
	}
	
	// Explorer tous les caracteres possibles dans la grille
	public void explore(Position p, Noeud n){
		
		// Sauvegarder le prefixe
		LinkedList<Character> savePrefix = new LinkedList<Character> (prefix);
		
		// Sauvegarder le masque
		boolean[][] saveMasque = new boolean [dim][dim];
		for (int i = 0 ; i < dim ; i++){
			for (int j = 0 ; j < dim ; j++){
				saveMasque[i][j] = masque[i][j];
			}
		}		
		// Prendre le caractere c a la position
		char c = p.e.grille[p.x][p.y];
		
		// Verifier que c peut etre ajoute au prefix
		Noeud nouveau = n.trouveFils(c);
		if (nouveau != null){
			
			// Ajouter c au prefixe courant
			prefix.add(c);
			
			// Modifier le masque
			masque[p.x][p.y] = true;
		
			// Verifier que le nouveau noeud courant marque la fin d un mot
			if (nouveau.estMot()){
				
				// Convertir le courant prefix en chaine
				String mot = versChaine(prefix);
				
				// Ajouter le mot a la liste des mots trouvees
				motsTrouves.add(mot);
			}
			
			// Parcourir toutes les positions possibles
			List<Position> liste = p.deplacementsLegaux();
			for (Position position : liste){
								
				// Poursuivre l exploration
				explore(position, nouveau);
			}
		}
		
		// Restaurer le prefixe
		prefix = savePrefix;
		
		// Restaurer le masque
		masque = saveMasque;
	}
	
	// Explorer un caractere dans la grille
	public List<String> exploreTout(){
		
		// Initialiser les variables d etats
		masque = new boolean[dim][dim];
		for (int i = 0 ; i < dim ; i++){
			for (int j = 0 ; j < dim ; j++){
				masque[i][j] = false;
			}
		}
		prefix = new LinkedList<Character> ();
		motsTrouves = new ArrayList<String> ();
		
		// Parcourir toutes la grille
		for (int i = 0 ; i < dim ; i++){
			for (int j = 0 ; j < dim ; j++){
				
				// Creer une position
				Position position = new Position(this, i, j);
				
				// Appeler la fonction explore
				explore(position, d.racine);
			}
		}
			
		// Trier les mots trouves
		triRapide(motsTrouves);	
			
		// Retourner les mots trouves
		return motsTrouves;
	}
	
	// Algorithme de tri rapide
	public static void triRapide(final ArrayList<String> tab){
		triRapideBorne(tab, 0, tab.size() - 1);
	}
	
	// Algorithme de tri rapide borne 
	public static void triRapideBorne(final ArrayList<String> tab, int g, int d){
		if (d <= g){
			return;
		} else {
			int indexPivot = partition(tab, g, d);
			if (g < indexPivot - 1){
				triRapideBorne(tab, g, indexPivot - 1);
			}
			if (indexPivot + 1 < d){
				triRapideBorne(tab, indexPivot + 1, d);
			}
		}
	}
	
	// Replacer de part et d autre du pivot
	public static int partition(final ArrayList<String> tab, int gauche, int droite){
		int g = gauche;
		int d = droite;
		String pivot = tab.get(gauche + (int) (Math.random() * (droite - gauche)));
		while ( g < d ){
			while ( (g < tab.size()) && ( (tab.get(g).length() < pivot.length()) || ( (tab.get(g).length() == pivot.length()) && (tab.get(g).compareTo(pivot) < 0) ) ) ) {
				g++;
			}
			while ( (d >= 0) && ( (tab.get(d).length() > pivot.length()) || ( (tab.get(d).length() == pivot.length()) && (tab.get(d).compareTo(pivot) > 0) ) ) ) {
				d--;
			}
			
			if (g < d){
				String tmp = tab.get(g);
				tab.set(g, tab.get(d));
				tab.set(d, tmp);
				
				// Pour eviter une boucle infinie, il faut verifier si les 2 mots sont les memes
				if (tab.get(g).compareTo(tab.get(d)) == 0){
					g++;
				}
			} 
		}
		return g;
	}
}


// Classe Position
class Position {
	
	// Variables immutables
  final int x, y;
  final Exploration e;
  
  // Constructeur
  public Position(Exploration e, int x, int y){
		this.e = e;
		this.x = x;
		this.y = y;
	}
	
	// Verifier si la position est legale
	private boolean estLegal(){
		return (x >= 0) && (x < e.dim) && (y >= 0) && (y < e.dim) && (!e.masque[x][y]);
	}
	
	// Renvoyer la liste de deplacement legaux a partir de la position courante
	public List<Position> deplacementsLegaux(){
		
		// Declare la liste
		LinkedList <Position> liste = new LinkedList <Position> ();
		
		// Parcourir toutes les positions possible
		for (int i = x - 1 ; i <= x + 1 ; i++){
			for (int j = y - 1 ; j <= y + 1 ; j++){
				
				// Creer une position
				Position position = new Position(e, i, j);
				
				// Verifier que la position est legale
				if (position.estLegal()){
					
					// Ajouter la position dans la liste
					liste.add(position);
				}
			}
		}
		
		// Retourner la liste
		return liste;
	}
}
