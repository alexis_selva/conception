# Devoir 7 : connections dans une carte

Connexions dans une carte
Cette semaine, pour prolonger le cours sur les graphes, nous allons nous intéresser à un graphe tiré du monde réel, qui contient les villes de France et de Mayotte. Le sujet vous permettra de vous familiariser avec une réprésentation possible des graphes (par liste d'adjacence), et d'écrire quelques algorithmes classiques : composantes connexes, recherche de chemins.

Obtenir le sujet
Le sujet est disponible en ligne. Téléchargez l'archive, puis extrayez son contenu dans le dossier de votre choix.

Aperçu du devoir
La classe que vous devrez compléter se trouve dans le fichier Test.java. Deux autres classes sont fournies : Ville et Carte. Il n'est pas nécessaire de modifier ces deux classes.

Nous vous fournissons dans ce devoir des cartes composées de villes, chacune munie d'un nom, d'une latitude et une longitude.

Il faut juste connaître quelques "méthodes" associées aux villes.

Pour chaque ville v,

on obtient son nom par v.getNom()
on obtient la distance avec une autre ville w par v.distance(w)
on obtient la latitude de la ville par v.getLatitude().
Le graphe
Vous sont données les cartes de Mayotte et de France. A chaque fois, vous obtenez la collection des villes par les instructions déjà écrites dans initMayotte et initFrance.

Nous allons transformer ces cartes en graphe non-orienté de la manière suivante :

les sommets sont des villes,
deux villes sont reliées par une arête si elles sont distantes de moins de minDist ; cette dernière distance paramètre donc le graphe.
Autrement dit, on peut se déplacer de ville en ville avec des "bottes de minDist lieues". Un tel graphe qui utilise les distances est dit graphe euclidien.

Représentation
Pour simplifier au maximum les structures Java utilisées, vous allez recopier les villes dans un tableau appelé villes. Vous dimensionnerez ce tableau à la bonne taille (là encore, l'instruction est donnée).

Comme vous disposez de ce tableau, vous pouvez ensuite, dans vos calculs, désigner les villes par l'indice correspondant dans le tableau.

Vous devrez ensuite construire et représenter les arêtes. Vous pouvez utiliser pour cela un autre tableau de listes de villes. Dans ce cas :

vous pouvez utiliser le type (ou classe) LinkedList qui est une définition de liste donnée dans la biblothèque Java.
Dans ce cas, pour chaque ville, vous devez avoir la liste de ses voisins.
Quand on utilise une LinkedList, il faut commencer par la créer comme new LinkedList();
Ensuite, on peut ajouter des éléments à une liste. Par exemple l.addFirst(e) ajoute e au début de la liste l.
Pour utiliser une liste, on peut parcourir ses éléments par (for Integer i : l) { ... }

Questions
Ce qui vous est demandé, est de compléter le fichier Test.java et pour cela :

de définir les fonctions qui construisent le graphe et le stockent dans les variables globales villes et voisins.
d'écrire la fonction compteCC qui compte le nombre de composantes connexes.
d'écrire la fonction relie qui teste s'il existe un chemin entre deux villes.
Optimisation
Vous remarquerez des indications dans le squelette de la fonction construitGraphe. Il s'agit d'une optimisation pour construire le graphe plus vite. Cette optimisation n'est pas nécessaire pour Mayotte, mais utile pour la France à cause du plus grand nombre de villes. L'idée de cette optimisation est que deux villes qui ont une latitude très différentes seront à une distance grande l'une de l'autre. En particulier, est calculé une valeur latDist; si la différence entre les latitudes de deux villes est plus grande que latDist, alors elles seront à une distance de plus de minDist. A vous d'utiliser cette remarque, par exemple en triant le tableau villes.
Taille de la pile
La manière la plus simple de programmer la parcours en profondeur (DFS) est d'utiliser une fonction récursive, comme montré dans les vidéos. Dans le cas de la carte de France, le grand nombre de villes fait que la taille allouée par défaut pour la pile des appels récursifs peut être insuffisante. Vous verrez vraisemblablement des messages d'erreurs indiquant un "Stack overflow" (débordement de pile). La manière simple d'y remédier, est de lancer votre programme en augmentant la taille de la pile. Il faut pour cela ajouter une option lors du lancement du programme. Une option -Xss4m alloue un espace mémoire de 4 méga-octets pour la pile, ce qui devrait être suffisant. Pour ajouter cette option :
Si vous lancez votre programme avec une ligne de commande, l'option est ajoutée dans la ligne de commande; par exemple : java -Xss4m Test.
Si vous travaillez dans l'environnement Netbeans, il faut :
Faire un clic droit sur le projet,
sélectionner Run,
ajouter l'option -Xss4m dans la case "VM options".
avec d'autres environnements intégrés, vous aurez certainement une manipulation similaire à faire.
