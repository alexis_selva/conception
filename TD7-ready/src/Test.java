import java.util.LinkedList;
import java.util.Collection;

public class Test {

  // pour charger les cartes
  static Carte carte;
  static final String chemin ="./"; // data file location
  Carte ens = new Carte(chemin+"mf.txt");

  final static int INEXPLORE = 0;
  final static int TRAITEMENT = 1;
  final static int EXPLORE = 2;

  static Ville[] villes;
  static LinkedList<Integer>[] voisins;

  static void construitGraphe(Collection<Ville> cv, double minDist) {
    // place dans le tableau villes, à qui on donnera la bonne dimension les
    // objets Ville de la collection cv
    int n = cv.size();
    villes = new Ville[n];

    // cette instruction cause un warning à la compilation
    // que vous pouvez ignorer
    voisins = new LinkedList[n];

    int k = 0;
    for (Ville v : cv) {
      villes[k] = v;
      k++;
    }

    // puis on place dans chaque voisins[i] la liste des numéros des voisins de
    // la ville villes[i] sont voisines d'une ville v, les villes qui sont à une
    // distance de moins de minDist. Il faut bien penser à initialiser voisins[j]
    // à newLinkedList() avant d'ajouter des éléments
    for (int i = 0 ; i < n ; i++) {
      voisins[i] = new LinkedList();
		}
		for (int i = 0 ; i < n ; i++) {
      for (int j = i + 1; j < n ; j++){ 
				if (villes[i].distance(villes[j]) < minDist) {
					voisins[i].addFirst(j);
					voisins[j].addFirst(i);
				}
			}
    }
  }

  // répond s'il existe un chemin de la villes[v1] à villes[v2] dans le graphe
  // défini par voisins
  static void DFS(LinkedList<Integer>[] voisins, int v) {
		villes[v].status = TRAITEMENT;
		for (int i : voisins[v]){ 
			if (villes[i].status == INEXPLORE) {
				DFS(voisins, i);
			}
		}
		villes[v].status = EXPLORE;
  }
  
  static boolean relie(Ville[] villes, LinkedList<Integer>[] voisins, int v1, int v2) {
		for (int i = 0 ; i < villes.length ; i++) {
			villes[i].status = INEXPLORE;
		}
		DFS(voisins, v1);
		return (villes[v2].status == EXPLORE);
	}


  static int compteCC(Ville[] villes, LinkedList<Integer>[] voisins) {
		int compteur = 0;
		for (int i = 0 ; i < villes.length ; i++) {
			if (villes[i].status == INEXPLORE){
				villes[i].status = TRAITEMENT;
				compteur++;
				DFS(voisins, i);
			}
		}
		return compteur;
  }

  static int premiereVille(String s) {
    for (int i = 0; i< villes.length; i++)
      if (s.equals(villes[i].getNom())) return (i);
    return(-1);
  }
  // retourne le numéro de la première ville du graphe dont le nom est s


  public static void initMayotte(double minDist){
    Carte ens = new Carte(chemin+"mf.txt");
    construitGraphe(ens.villes(), minDist);
  }

  public static void initFrance(double minDist){
    Carte ens = new Carte(chemin+"fr.txt");
    construitGraphe(ens.villes(), minDist);

  }


  public static void test1(double minDist) {
    System.out.println("Mayotte, pas de "+(int)minDist);
    initMayotte(minDist);

    int v1 = premiereVille("Accua") ;
    int v2 = premiereVille("Moutsamoudou");
    int v3 = premiereVille("Bandraboua");
    int v4 = premiereVille("Mambutzou");


    System.out.println("nb composantes : "+compteCC(villes, voisins));
    System.out.println(relie(villes, voisins, v1, v2));
    System.out.println(relie(villes, voisins, v1, v3));
    System.out.println(relie(villes, voisins, v2, v3));
    System.out.println(relie(villes, voisins, v2, v4));

  }

  public static void test2(double minDist) {

    System.out.println("France, pas de "+minDist);

    initFrance(minDist);
    System.out.println("composantes : "+compteCC(villes, voisins));
  }

  public static void main (String[] args) {

    //test1(1850);
    //test1(2000);
    //test1(3000);
    //test1(3400);
    //test1(4000);

    // tests sur la carte de France
    // peuvent être longs voire demander d'augmenter la taille de la pile
    //test2(2000);
    test2(5000);

  }

}
