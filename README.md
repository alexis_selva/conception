Voici les devoirs de programmation portant sur la conception d'algorithmes :

* Devoir 1 : algorithmes de compression
* Devoir 2 : tables de hachage
* Devoir 3 : recherche de mots dans une grille
* Devoir 4 : Recherche de mots dans une grille - partie 2
* Devoir 5 : moindres carrés, par segments
* Devoir 6 : résolution d'une grille de Sudoku
* Devoir 7 : connections dans une carte
* Devoir 8 : distances dans la carte

Pour plus d'information, je vous invite à jeter un coup d'oeil à l'adresse https://www.coursera.org/course/algoprog