# Devoir 8 : distances dans la carte

Le dernier devoir reprend les cartes de Mayotte et de France, ainsi que le principe de graphe euclidien : deux villes sont reliées si elles sont à une distance inférieure à un paramètre donné.

L'énoncé est ici.

Par rapport à la semaine 7, on utilisera des HashMap et des HashSet pour représenter les ensembles de voisins. On le fera aussi pour représenter l'ensemble des villes portant un nom donné.

Remarquez que aussi que un fichier VilleDist.java vous est donné. Vous pouvez choisir de l'utiliser ou pas.

On rappelle que le fichier Ville.java permet d'ordonner les villes par latitude croissante, ce qui permet d'accélérer la construction du graphe.
Vous serez libres dans la manière d'implémeter l'algorithme de Dijkstra.

Bonne chance !
