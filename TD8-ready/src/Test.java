import java.util.LinkedList;
import java.util.Collection;
import java.util.PriorityQueue;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Arrays;
import java.text.DecimalFormat;
import java.util.*;

public class Test {

    // pour charger les cartes
    static Carte carte;
    static final String chemin ="./"; // data file location
    Carte ens = new Carte(chemin+"mf.txt");
    static HashMap<Ville, HashSet<Ville>> voisins;
    static HashMap<String, HashSet<Ville>> nom;

	static void construitGraphe(Collection<Ville> cv, double minDist) {

		// des villes à moins de minDist l'une de l'autre auront au plus une différence de
		// latitude (ou de longitude) de latDist 
		double R = 6371000;
		double latDist = minDist * 180.0 / Math.PI / R;
					
		// indication : on peut trier un tableau de villes par Array.sort
		int n = cv.size();
		Ville[] villes = new Ville[n];
		int k = 0;
		for (Ville v : cv) {
			villes[k] = v;
			k++;
		}
		Arrays.sort(villes, new Comparator<Ville>() {
			@Override
			public int compare(Ville v1, Ville v2) {
				return v1.compareTo(v2);
			}
		});
		
		// ici construire le graphe des villes en mettant les bonnes valeurs 
		// dans les tables voisins et nom
		voisins = new HashMap<Ville, HashSet<Ville>>();
		nom = new HashMap<String, HashSet<Ville>>();
		for (int i = 0 ; i < n ; i++) {
			voisins.put(villes[i], new HashSet<Ville>());
			HashSet<Ville> set = new HashSet<Ville>();
			set.add(villes[i]);
			nom.put(villes[i].getNom(), set);
		}
		
		// Parcourir toutes les villes
		for (int i = 0 ; i < n ; i++) {				
				
				// Declarer j
				int j = i + 1;
				
				// Determiner les villes dont la latitude sont comprises entre latitude - latDist et latitude + latDist
				while ( (j < n) && ( ( (villes[i].getLatitude() + latDist) >= villes[j].getLatitude() )	&& ( (villes[i].getLatitude() - latDist) <= villes[j].getLatitude() ) ) ) {
				
					// Est ce un voisin ?
					if (villes[i].distance(villes[j]) <= minDist){
							
						// Recuper la liste des voisins de i
						HashSet<Ville> set = voisins.get(villes[i]);
							
						// Ajouter j
						set.add(villes[j]);
							
						// Mettre a jour la liste des voisins et des noms de i
						voisins.put(villes[i], set);
							
						// Recuper la liste des voisins de j
						set = voisins.get(villes[j]);
							
						// Ajouter i
						set.add(villes[i]);
							
						// Mettre a jour la liste des voisins et des noms de j
						voisins.put(villes[j], set);
					}
					
					// Incrementer j
					j++;
				} 
			}
		}

    static Ville premiereVille(String s) {
			return(nom.get(s).iterator().next());
    }

    static double Dijkstra(Ville orig, Ville dest) {
			// utiliser Dijkstra pour calculer la longueur du plus court chemin
			// entre v1 et v2
			// rendre -1 s'il n'y a pas de chemin
			
			// Initialisation
			VilleDist vd;
			Ville villeCourant;
			double distCourant = 0.0;
			HashMap<Ville, Double> distances = new HashMap<Ville, Double>();
			PriorityQueue<VilleDist> file = new PriorityQueue();
			file.add(new VilleDist(orig, 0.0));
			
			// Repeter tant que la file n est pas vide
			while (!file.isEmpty()){
				
				// Enlever un element de la file
				vd = file.remove();
				villeCourant = vd.v;
				distCourant = vd.d;		
				
				// Verifier si on a atteint la destination
				if (villeCourant == dest) {
					return distCourant;
				}
				
				// Verifier que la ville courant n a pas deja ete traite
				if (!distances.containsKey(villeCourant)){
					
					// Ajouter la ville courante aux distances
					distances.put(villeCourant, distCourant);
					
					// Parcourir en largeur tous les voisins
					for (Ville voisin : voisins.get(villeCourant)){
												
						// Verifier si le voisin n a pas deja ete traite
						if (!distances.containsKey(voisin)){
							
							// Ajouter le voisin dans la file
							file.add(new VilleDist(voisin, distCourant + villeCourant.distance(voisin)));
						}
					}
				}
			}

			// La destination n a pas ete atteinte
      return -1;
    }

		public static void initMayotte(double minDist){
			Carte ens = new Carte(chemin+"mf.txt");
			construitGraphe(ens.villes(), minDist);
		}

		public static void initFrance(double minDist){
			Carte ens = new Carte(chemin+"fr.txt");
			construitGraphe(ens.villes(), minDist);
		}

    public static void test1(double minDist) {
			System.out.println();
    	System.out.println("Mayotte, pas de "+minDist);
    	initMayotte(minDist);

    	Ville v1 = premiereVille("Accua") ;
    	Ville v2 = premiereVille("Moutsamoudou");
    	Ville v3 = premiereVille("Bandraboua");
			Ville v4 = premiereVille("Mambutzou");
			afficheDijkstra(v1, v2);
			afficheDijkstra(v2, v1);
			afficheDijkstra(v1, v3);
			afficheDijkstra(v3, v1);
			afficheDijkstra(v1, v4);
			afficheDijkstra(v4, v1);
			afficheDijkstra(v2, v3);
    }

    public static void afficheDijkstra(Ville v1, Ville v2) {
			DecimalFormat df = new DecimalFormat("#.000");
			double d = Dijkstra(v1,v2);
			String s = "  pas de chemin";
			if (d > 0) s = df.format(Dijkstra(v1,v2) / 1000);
			System.out.println(v1.getNom()+" "+v2.getNom()+" "+s);
    }

    public static void test2(double minDist) {
			System.out.println();
    	System.out.println("France, pas de "+minDist);

    	initFrance(minDist);

    	Ville paris = premiereVille("Paris") ;
    	Ville rouen = premiereVille("Rouen");
			Ville palaiseau = premiereVille("Palaiseau");
			Ville perpignan = premiereVille("Perpignan");
			Ville strasbourg = premiereVille("Strasbourg");
			Ville hagenau = premiereVille("Hagenau");
			Ville brest = premiereVille("Brest");
			Ville hendaye = premiereVille("Hendaye");
		
    	afficheDijkstra(paris, rouen);
    	afficheDijkstra(palaiseau, rouen);
    	afficheDijkstra(palaiseau, paris);
			afficheDijkstra(paris, perpignan);
			afficheDijkstra(hendaye, perpignan);
			afficheDijkstra(paris, strasbourg);
			afficheDijkstra(hagenau, strasbourg);
			afficheDijkstra(hagenau, brest);
    }

    public static void main (String[] args) {

    	//test1(1850);
    	//test1(2000);
    	//test1(3000);
    	//test1(3400);
    	//test1(4000);

			//tests sur la carte de France
			//test2(2000);
			//test2(5000);
			//test2(7000);
			test2(10000);
    }
}
