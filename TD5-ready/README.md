# Devoir 5 : Moindres carrés, par segments 

Programmation dynamique
Nous allons cette semaine nous intéresser à un problème géométrique qui peut se résoudre à l'aide d'une technique de programmation dynamique. Ce problème est connu sous le nom de « segmented least squares » dans la littérature. En français, nous dirions « moindres carrés par segments ». Les explications ci-dessous reprennent en grande partie le contenu de la vidéo présentant le devoir. Si cela ne vous suffit toujours pas, vous trouverez aisément des informations complémentaires en cherchant un peu sur internet. Si vous y avez accès, l'ouvrage de Kleinberg et Tardos « Algorithm design » contient une bonne description de cet algorithme. À défaut, les transparents sont disponibles en anglais.

Obtenir le sujet / soumission des résultats
L'archive est disponible en ligne. Téléchargez-la, puis extrayez son contenu dans le dossier de votre choix.

La procédure de soumission suit le même protocole que pour les devoirs précédents : des fonctions de test à décommenter et à ne surtout pas modifier, et la sortie de votre programme qui sera validée ou non par Coursera.

En cas de problème
Si vous rencontrez des soucis techniques avec le processus de soumission, ou si le sujet vous semble ambigu ou contenir une erreur, les forums du cours sont à votre disposition pour demander de l'aide.

Si vous rencontrez des soucis techniques pour mettre en place votre environnement de développement Java, de nombreux documents de référence sont disponibles en ligne.

Description du problème
Nous avons une suite de points dans le plan décrits par leurs coordonnées.


(x1,y1),…,(xn,yn)
Les xᵢ et yᵢ seront représentés par des nombres flottants, c'est-à-dire des double en Java. Plus précisément, nous aurons deux tableaux de double nommés xtab et ytab, de même longueur. Pour chaque point i, ses deux coordonnées seront données par xtab[i] et ytab[i]. On suppose que xtab est trié par ordre croissant, c'est-à-dire que les points 1, 2, ... , n sont placés de gauche à droite dans le plan.



On suppose que les points sont à peu près placés sur un certain nombre de segments. Par exemple dans le cas de la figure ci-dessus, les points sont placés sur 2 segments ; dans le cas de la figure ci-dessous, sur 3 segments.



Le but est d'écrire un programme qui détermine le nombre de segments.

Structure du sujet
Dans un premier temps, pour nous familiariser avec le problème, nous allons parler de régression linéaire, c'est-à-dire du cas où il n'y a qu'un seul segment. Nous verrons comment trouver la formule analytique de ce segment et l'erreur qui en découle. Nous pourrons ensuite étendre le problème et nous intéresser au cas où plusieurs segments doivent être trouvés. Nous verrons alors en quoi ce problème relève de la programmation dynamique.

Problème à un segment : régression linéaire
Pour déterminer le nombre de segments, nous aurons besoin de quelques formules simples de statistiques. Commençons par celles qui permettent de trouver une droite qui passe le plus près possible d'un ensemble de points. C'est la technique de la formule de la régression linéaire.

Ces formules peuvent être utilisées sans chercher à en comprendre tous les détails.
Une droite est donnée par la formule:

y=a⋅x+b
Si on a l'ensemble de points suivant :

(x1,y1),(x2,y2),…,(xn,yn)
alors les coefficients a et b de la droite passant au plus près des points sont donnés par les formules :


a=nΣixiyi−(Σixi)(Σiyi)nΣix2i−(Σixi)2

b=Σiyi−aΣixin
Cette droite passe au plus près des points, en ce qu'elle minimise l'erreur. L'erreur est obtenue en additionnant pour chaque (xᵢ, yᵢ) la différence entre yᵢ et a·xᵢ + b, ou plutôt les carrés des erreurs :


err=Σi(yi−axi−b)2
L'utilisation d'un carré permet de ne pas accorder trop d'importance à des points isolés présentant de gros écarts.

Fichiers .java
Le problème étudié dans ce devoir ne nécessite pas une structure objet très compliquée. Nous aurons donc une seule classe, appelée Seg, que vous trouverez dans le fichier src/Seg.java. Le fichier contient aussi un pseudo-générateur de nombres aléatoires, qui permet de garantir que tout le monde aura les mêmes séries de nombres générées.

Calcul de l'erreur
Écrivez une fonction

  static double[] erreur(double[] xtab, double[] ytab, int d, int f)
  Cette fonction opère sur les points situés dans l'intervalle [d, f], c'est-à-dire allant des points d à f inclus.
  
  Cette fonction calcule les coefficients a et b sur le sous-ensemble de points, ce qui permet ensuite de calculer l'erreur que fait la droite passant au plus près des points :
  
  
  (xd,yd),…,(xf,yf)
  qui sont donnés par les tableaux xtab et ytab. La fonction doit donc renvoyer un tableau de trois double qui contient, dans l'ordre: a, b, et l'erreur.
  
  Nous rappelons qu'une manière simple de renvoyer un tableau de trois éléments en Java est d'utiliser la syntaxe return new double[] { a, b, err };.
  
  La fonction devra lancer une exception de votre choix (par exemple IllegalArgumentException) si les tableaux sont de longueurs différentes, si d ou f sont en dehors des bornes du tableau, ou si f est plus petit que d.
  
  Soumission de vos résultats
  Les deux fonctions test1 et test2 sont faites avec des jeux de tests très simples, qui vous permettront de débugger votre code aisément. Par exemple, la solution de test1 est un segment qui relie les deux points ; l'erreur sera donc nulle. Pour test2, la solution n'est pas celle à laquelle on pourrait penser intuitivement (droite y = 0). En effet, la méthode des moindres carrés va accorder moins d'importance au point situé en (1, 1).
  
  La fonction test3 contient un jeu de points situés vaguement autour d'une droite de pente 10. Votre coefficient a devrait donc se trouver aux alentours de 10.
  
  Soumettez test1, test2 et test3.
  
  Problème général
  Dans le cas de la figure 1, si l'on cherche la droite approximant tous les points, on aura une erreur très grande. En revanche, si on partage l'ensemble des points entre p₁ à p₅ d'une part, et p₆ et p₁₂ d'autre part, on aura deux segments qui donneront des erreurs faibles.
  
  Si l'on s'autorise un trop grand nombre de segments, le problème devient trop facile, car en faisant passer les segments juste par deux points successifs à chaque fois, on a une erreur nulle. On va donc compter une pénalité C pour chaque droite supplémentaire. La valeur de C dépend du problème. Dans le cas de ce devoir, on prendra C = 200.
  
  Le but est donc de trouver une manière de découper l'ensemble des points en séquence successives de segments S₁, …, Sn tel que la sommes des erreurs pour chaque segment, plus la pénalité, c'est-à-dire k·C pour les k segments soit minimale. En d'autre termes, si errᵢ est l'erreur du i-ième segment, on veut minimiser :
  
  
  k×C+Σki=1erri
  Calcul du nombre de segments
  Écrivez une fonction :
  
    static int nbSeg (double[] xtab, double[] ytab, double c)
    qui retourne le nombre de séquences (presque) alignées dans les points donnés par les tableaux xtab et ytab, en utilisant le coût c pour chaque ligne supplémentaire.
    
    Voici quelques indications.
    
    Le dernier point pn appartient à la dernière séquence. Appelons pᵢ le premier point de cette dernière séquence. Alors, le coût de la meilleure manière de découper les points est la somme de :
    
    la valeur calculée par la fonction erreur pour les points i à n,
    la pénalité C pour ce segment de i à n,
    le coût pour le meilleur découpage possible des points 1 à i-1.
    Voici quelques conseils pratiques.
    
    Le sujet est formulé avec des indices allant de 1 à n. En informatique, les indices vont de 0 à n-1, attention à ne pas confondre !
    Essayez de bien spécifier vos fonctions. Il est plus simple de raisonner pour les indices allant de 0 à i inclus.
    Soumission des résultats
    De nouveau, nous fournissons deux fonctions relativement simples pour vous permettre de tester vos résultats. La fonction test4 doit renvoyer, naturellement, un seul segment, puisqu'il n'y a que deux points.
    
    La fonction test5, quant à elle, est plus intéressante. Vous pourrez remarquer que dans la formulation du problème ci-dessus, nous ne connectons pas les segments. En d'autres termes, nous n'imposons une "ligne brisée" (fonction linéaire par morceaux), mais une succession de segments. En effet, la solution optimale s'arrête au point i-1, et nous faisons commencer le segment suivant au point i.
    
    Le jeu de tests utilisé pour la fonction test5 est constitué de quatre points. La fonction commence par calculer l'erreur associée à un segment couvrant tous les points : vous devez trouver 3.20. Le coût total de cette solution est donc 3.20 + 1×C où C est la pénalité. Une autre solution que votre algorithme doit envisager est est une succession de deux segments reliant les points 1 et 2, puis les points 3 et 4. Les segments, puisqu'ils sont constitués de deux points, ont chacun une erreur de 0. Le coût total est donc 0 + 0 + 2×C.
    
    Le premier appel à nbSeg prend C = 3. La solution à 1 segment (coût 6.2) est donc moins bonne que la solution à 2 segments (coût 6).
    Le second appel, en revanche, choisit C = 4. La solution à 1 segment (coût 7.2) devient donc plus économe que la solution à 2 segments (coût 8).
    La fonction test6 utilise des points générés aléatoirement.
    
    Soumettez les fonction test4, test5, et test6.
