public class Test {

  public static void main(String[] args) {
    //test1();
    //test2();
    //test3();
    //test4();
    test5();
    //test6();
  }

  public static void test1() {
    // Une fonction de test très simple pour vous aider à débugger votre code.
    double xtab[] = new double[] {  0, 1 };
    double ytab[] = new double[] { -1, 1 };
    double[] ret = Seg.erreur(xtab, ytab, 0, 1);
    System.out.printf("test1 a=%.2f, b=%.2f, e=%.2f\n", ret[0], ret[1], ret[2]);
  }

  public static void test2() {
    // Une fonction de test très simple pour vous aider à débugger votre code.
    double xtab[] = new double[] {  0, 1,  2 };
    double ytab[] = new double[] { -1, 1, -1 };
    double[] ret = Seg.erreur(xtab, ytab, 0, 2);
    System.out.printf("test2 a=%.2f, b=%.2f, e=%.2f\n", ret[0], ret[1], ret[2]);
  }

  public static void test3() {
    double xtab[] = new double[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
    double ytab[] = new double[] { 7.153, 12.555, 25.835, 40.771, 42.252, 54.797, 67.178, 76.227, 83.967, 92.970, 106.734 };
    double[] ret = Seg.erreur(xtab, ytab, 0, 2);
    System.out.printf("test3 a=%.2f, b=%.2f, e=%.2f\n", ret[0], ret[1], ret[2]);
  }

  public static void test4() {
    // Une fonction de test très simple pour vous aider à débugger votre code.
    double xtab[] = new double[] {  0, 1 };
    double ytab[] = new double[] { -1, 1 };
    System.out.printf("test4 n=%d\n", Seg.nbSeg(xtab, ytab, 200));
  }

  public static void test5() {
    // Une fonction de test très simple pour vous aider à débugger votre code.
    double xtab[] = new double[] {  0, 1,  2, 3 };
    double ytab[] = new double[] { -1, 1, -1, 1 };
    double[] ret = Seg.erreur(xtab, ytab, 0, 3);
    System.out.printf("test5-0 a=%.2f, b=%.2f, e=%.2f\n", ret[0], ret[1], ret[2]);
    System.out.printf("test5-1 n=%d\n", Seg.nbSeg(xtab, ytab, 3));
    System.out.printf("test5-2 n=%d\n", Seg.nbSeg(xtab, ytab, 4));
  }

  public static void test6() {
    final int n = 1000;
    double xtab [] = new double[n];
    double ytab [] = new double[n];
    for (int k=0; k<12; k++) {
      TestHelpers.iterate(xtab, ytab);
      System.out.printf("test6-%d n=%d\n" , k, Seg.nbSeg(xtab, ytab, 200));
    }
  }

}

// Classe permettant de générer des tableaux de test aléatoires. Ne pas toucher !
class TestHelpers {

  static double newB (double a, double na, double b, double x) {
    return (a * x + b - na * x) ;
  }

  static void generate (int n, int c, double a, double b, double x, double[] xtab, double[] ytab) {
    for (int i = 0; i < n; i++) {
      x = x + Prng.next() * 20;
      xtab[c] = x;
      ytab[c] = x * a + b + 9 * (Prng.next() - 0.5);
      c++;
    }
    return;
  }

  static void iterate (double[] xtab, double[] ytab) {
    int c = 0;
    int n = 0;
    double a = 1;
    double na;
    double b = 0;
    xtab[0] = 0;
    ytab[0] = 0;
    while (c < xtab.length -1) {
      n =  (int)(Prng.next() * 80.0)+16 ;
      if (n+c >= xtab.length - 80) {
        n = xtab.length -c - 1;
        generate (n, c, a, b, xtab[c], xtab, ytab);
        break;
      }
      if (n+c >= xtab.length) n =  xtab.length -c - 1;
      do {
        na = (Prng.next() - 0.5) * 12;
      } while (Math.abs(a-na) < 2) ;
      b = newB(a, na, b, xtab[c]);
      a = na;
      generate (n, c, a, b, xtab[c], xtab, ytab);
      c = c + n;
    }
  }

}
