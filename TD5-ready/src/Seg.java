class Seg {

	// Calculer a, b et l erreur
  static double[] erreur (double[] xtab, double[] ytab, int d, int f) {
		
		// Verifier les parametres
    if (xtab == null){
			throw new IllegalArgumentException("Le tableau de x specifie en entree est nul");
		}
		if (ytab == null){
			throw new IllegalArgumentException("Le tableau de y specifie en entree est nul");
		}  
		if (xtab.length != ytab.length){
			throw new IllegalArgumentException("Le tableau de x n a pas la meme taille que celui de y");
		}
		if (xtab.length == 0){
			throw new IllegalArgumentException("Les tableaux sont de taille nulle");
		}
		if (d > f){
			throw new IllegalArgumentException("L indice d est superieur a f");
		}
		if (f >= xtab.length){
			throw new IllegalArgumentException("L indice f est superieur ou egal a la taille des tableaux");
		}
		
		// Calculer n
		int n = f - d + 1;
		
		// Calculer sumXi
		double sumXi = 0.0;
		for (int i = d ; i <= f ; i++){
			sumXi += xtab[i];
		}
		
		// Calculer sumYi
		double sumYi = 0.0;
		for (int i = d ; i <= f ; i++){
			sumYi += ytab[i];
		}
		
		// Calculer sumXiYi
		double sumXiYi = 0.0;
		for (int i = d ; i <= f ; i++){
			sumXiYi += (xtab[i] * ytab[i]);
		}
		
		// Calculer sumXiCarre
		double sumXiCarre = 0.0;
		for (int i = d ; i <= f ; i++){
			sumXiCarre += (xtab[i] * xtab[i]);
		}
		
		// Calculer carreSumXi
		double carreSumXi = sumXi * sumXi;
		
		// Calculer a
		double a = (n * sumXiYi - sumXi * sumYi) / (n * sumXiCarre - carreSumXi);

		// Calculer b
		double b = (sumYi - a * sumXi) / n;
		
		// Calculer err
		double err = 0.0;
		for (int i = d ; i <= f ; i++){
			double sum = ytab[i] - a * xtab[i] - b;
			err += (sum * sum);
		}
		
		// Retourner les resultats
    return new double[] { a, b, err };
  }
  
  // Calculer le cout d un segment
  static double cout(double[] xtab, double[] ytab, double c, int d, int f){
		double err[] = erreur(xtab, ytab, d, f);
		return err[2] + c;
	}


	// Calculer le nombre de segments qui minimisent l erreur
  static int nbSeg (double[] xtab, double[] ytab, double c) {
				
		// Initialiser les tableaux de memorisation
		double min[] = new double[xtab.length];
		int segments[] = new int[xtab.length];
		
		// Calculer n
		final int n = xtab.length - 1;
		
		// initialiser les valeurs a la position n
		min[n] = 0;
		segments[n] = 1;
		
		// Parcourir tous les indices du tableau
		for (int i = n; i >= 0 ; i--){
								
			// Calculer l erreur entre I et N
			min[i] = cout(xtab, ytab, c, i, n);
			
			// Initialiser le nombre de segments
			segments[i] = 1;

			// Parcourir tous les indices entre I et N - 1
			for (int j = n - 1; j > i ; j--){
				
				// Calculer l erreur entre I et J
				double err = cout(xtab, ytab, c, i, j);
				
				// Evaluer le cout minimal
				if (min[j + 1] + err < min[i]){
					
					// Mettre a jour les tableaux pour la memorisation
					min[i] = min[j + 1] + err;
					segments[i] = segments[j + 1] + 1;
				}		
			}
		}
		
		// Retourner le nombre de segments qui minimisent les erreurs
		return segments[0];
  }
}


class Prng {
  static double seed = 0.4435431;
  static int m = 1;
  static double a = 68.45633;
  static double b = 0.724514 ;

  static double next() {
    seed = (double) (((seed) * a + b) % m);
    return seed;
  }
}
